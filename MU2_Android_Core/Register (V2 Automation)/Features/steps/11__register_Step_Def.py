from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Env import driver
import datetime
from Features.steps.Function import date_of_birth_widget
from Features.steps.Function import scroll_down_to_element
from Features.steps.Function import scroll_up_to_element

use_step_matcher("re")


@given("register user screen shows")
def step_impl(context):
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Ensures that the sign in page has loaded successfully by locating the login logo via the elements ID when it appears

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

        scroll_down_to_element("fragment_login__textview_create_account")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        createAccountButton = driver.find_element_by_id("fragment_login__textview_create_account")
        createAccountButton.click()
        # Finds the resource ID of the create account link and clicks this to get to the registration page

        WebDriverWait(driver, 5).until(
            EC.presence_of_element_located((By.ID, "fragment_registration__inputfield_fullname")))
        # Ensures that the registration page has loaded successfully by locating the first field via its ID once the element is shown

        registerTitle = driver.find_element_by_id("fragment_registration__inputfield_fullname")
        registerTitle.is_displayed()
        # Finds the resource ID of the registration page title and checks that this is displayed

    except:
        if email.is_displayed():
            # If the email field is displayed then the rewards pop up did not show

            scroll_down_to_element("fragment_login__textview_create_account")
            # Scroll down Function is called from the Function.py file that can be found in the steps folder

            createAccountButton = driver.find_element_by_id("fragment_login__textview_create_account")
            createAccountButton.click()
            # Finds the resource ID of the create account link and clicks this to get to the registration page

            WebDriverWait(driver, 5).until(
                EC.presence_of_element_located((By.ID, "fragment_registration__inputfield_fullname")))
            # Ensures that the registration page has loaded successfully by locating the first field via its ID once the element is shown

            registerTitle = driver.find_element_by_id("fragment_registration__inputfield_fullname")
            registerTitle.is_displayed()
            # Finds the resource ID of the registration page title and checks that this is displayed

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@step('name field is filled with "(?P<Full_name>.+)"')
def step_impl(context, Full_name):
    if Full_name == "(leave blank)":
        pass
        # If the Data set for the name is left blank then this step is skipped to leave the field blank

    else:
        fullNameField = driver.find_element_by_id("fragment_registration__inputfield_fullname")
        fullNameField.send_keys(Full_name)
        # If there should be an input for the full name. Then this data is sent to the field via the send keys command

        driver.hide_keyboard()
        # Keyboard is then closed so the full screen of the device can be utilised


@step('email field is filled with "(?P<Email>.+)"')
def step_impl(context, Email):
    if Email == "(leave blank)":
        pass
        # If the Data set for the email is left blank then this step is skipped to leave the field blank

    else:
        emailField = driver.find_element_by_id("fragment_registration__inputfield_email")
        # Finds the email field in the registration page via the element's resource ID

        t = datetime.datetime.now()
        ti = str(t)
        ti = ti.replace(" ", "")
        ti = ti.replace("-", "")
        ti = ti.replace(":", "")
        ti = ti.replace(".", "")
        ti = ti[0:12]
        si1 = (ti + Email)
        # A timestamp is captured and is formatted to only contain numbers and is added to the front of the email address

        emailField.send_keys(si1)
        # The timestamped email is then sent the email field to ensure a successful registration

        driver.hide_keyboard()
        # Keyboard is closed to utilise the devices full screen


@step('mobile number field is filled with "(?P<Mobile>.+)"')
def step_impl(context, Mobile):
    if Mobile == "(leave blank)":
        pass
        # If the Data set for the Mobile number is left blank then this step is skipped to leave the field blank

    else:
        mobileField = driver.find_element_by_id("fragment_registration__inputfield_phonenumber")
        # Mobile number field is found via its resource ID if the data set is not set to be left blank

        mobileField.send_keys(Mobile)
        driver.hide_keyboard()
        # The Mobile number held in the data set is inputted into the mobile number field via the send keys command


@step('password field is filled with "(?P<Password>.+)"')
def step_impl(context, Password):

    if Password == "(leave blank)":
        # This checks whether the dataset for this field is meant to left blank.

        driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
        pass
        # If this field is to be left blank. A touch action is sent to scroll down so the next step can begin quicker

    elif Password != "(leave blank)":
        # This checks whether the dataset for this field contains a password that should be inputted

        Field = driver.find_element_by_id("fragment_registration__inputfield_password")
        Field.send_keys(Password)
        # Finds password field element by its resource ID and sends password in the dataset through via send keys command

        driver.hide_keyboard()
        # Keyboard is then hide so the full screen of the device can be utilised

        driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
        # Touch action is sent to scroll down the registration page to the customer reference number field.


@step('Customer reference field is filled with "(?P<Customer_reference_number>.+)"')
def step_impl(context, Customer_reference_number):

    if Customer_reference_number == "(leave blank)":
        # Checks the dataset to see if this field should be left blank

        driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
        pass
        # If the field is going to be left blank. A touch action is used here to scroll down the page to the next element

    elif Customer_reference_number != "(leave blank)":
        # Checks the dataset to see of this field should not be left blank

        scroll_down_to_element("fragment_registration__inputfield_crn")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        crnField = driver.find_element_by_id("fragment_registration__inputfield_crn")
        # Locates the customer reference field via the elements resource ID

        crnField.send_keys(Customer_reference_number)
        # Inputs the customer reference number in this field with the data found in the dataset

        driver.hide_keyboard()
        # Keyboard this then hidden to utilise the devices full screen


@step('date of birth field is filled with "(?P<DoB>.+)"')
def step_impl(context, DoB):

    if DoB == "(leave blank)":
        # Checks whether the DOB in the dataset to see if this field should be left blank

        driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
        pass
        # If this field is left blank. A Touch action used here to scroll down the page so the next field can be found quicker

    else:
        dobField = driver.find_element_by_id("fragment_registration__inputfield_dob")
        dobField.click()
        # Locates date of birth element via the elements resource ID.
        # Once located, the element is clicked

        date_of_birth_widget(DoB)
        # date of birth widget Function is called from the Function.py file that can be found in the steps folder
        # The desired date will be passed from the feature file to this function


@step('postcode is filled with "(?P<Postcode>.+)"')
def step_impl(context, Postcode):

    if Postcode == "(leave blank)":
        # Checks whether this field should be left blank based on the dataset

        driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
        pass
        # A touch action is used to scroll down the page so the element in the next step can be found quicker.

    elif Postcode != "(leave blank)":
        # If there is a postcode to input. This path will be followed

        scroll_down_to_element("fragment_registration__inputfield_postcode")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        postcodeField = driver.find_element_by_id("fragment_registration__inputfield_postcode")
        postcodeField.send_keys(Postcode)
        # Element is then located by its resource ID and the postcode is inputted via the send keys command

        driver.hide_keyboard()
        # Keyboard is then hidden to utilise the full screen of the device


@step("Terms and Conditions field is ticked")
def step_impl(context):
    scroll_down_to_element("fragment_registration__button_signup")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    try:
        CorrectCheckBox = False
        # False boolean value used for loop that determines the text of the relevant checkbox

        n = 0
        # Sets "n" to a value of "0" for use in a loop that requires an index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:11] != "I have read":
                # List of elements containing the checkbox ID is created. The text on the element is shortened and compared to this hard coded text

                n = n + 1
                if n == 2:
                    raise Exception("Terms and conditions acceptance checkbox could not be located. Text could not be matched with any of the text attribute of the elements in the list")
                # If the text is not matched the index value is increased by 1 up to a range of 3 as they are only a few checkboxes on the page

            elif allCheckBoxes[n].text[0:11] == "I have read":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    pass
                # If the response provides a true string value. The element is already checked. Then the step can be passed

                elif isChecked == "false":
                    raise Exception("Terms and conditions acceptance checkbox is not automatically checked")
                # If the response provides a false string value. An exception is raised as this element should be checked by default

    except:
        if driver.find_element_by_id("fragment_registration__textview_terms_conditions").text == "Data not available":
            raise Exception("Terms and conditions data did not load. Maybe the API is not working or the network connectivity is not good enough")
        # If an exception occurs here. It will be because the terms and conditions data had not loaded in as the terms and conditions text will show "Data not available"

        else:
            raise Exception("Terms and conditions checkboxes could not be located by their shared ID")
        # if an exception occurs here. The element could not be located by its ID. The elements ID may have changed


@step("allowing Utilita to collect data field is ticked")
def step_impl(context):
    n = 0
    # Sets "n" to a value of "0" for use in a loop that requires an index value

    CorrectCheckBox = False
    # False boolean value used for loop that requires and indexed value

    allCheckBoxes = driver.find_elements_by_id("checkbox_field")
    while not CorrectCheckBox:
        if allCheckBoxes[n].text[0:12] != "I understand":
            # List of elements containing the checkbox ID is created. The text on the element is shortened and compared to this hard coded text

            n = n + 1
            if n == 2:
                n = 0
            # If the text is not matched the index value is increased by 1 up to a range of 3 as they are only a few checkboxes on the page

        elif allCheckBoxes[n].text[0:12] == "I understand":
            CorrectCheckBox = True
            # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

            isChecked = allCheckBoxes[n].get_attribute('checked')
            # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

            if isChecked == "true":
                pass
            # If the response provides a true string value. The element is already checked. Then the step can be passed

            elif isChecked == "false":
                raise Exception("Data Consent acceptance checkbox is not automatically ticked")
            # If the response provides a false string value. An exception is raised as this element should be checked by default


@step('Marketing field is "(?P<Marketing_consent>.+)"')
def step_impl(context, Marketing_consent):
    if Marketing_consent == "not ticked":
        # This checkbox unlike the previous two will be un-ticked and we will assume if dataset asks us to leave this checkbox then the step can be passed
        CorrectCheckBox = False
        # False boolean value used for indexed loop

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:8] != "I opt in":
                # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                n = n + 1
                if n == 3:
                    n = 0
                # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:8] == "I opt in":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    raise Exception("Marketing consent checkbox should not be automatically checked")
                    # If the response provides a true string value. An Exception is raised as this element should not be automatically checked.
                    # Due to GDPR we must ensure that the user has actively chosen to receive marking information

                elif isChecked == "false":
                    pass
                    # if the response provides a false string value. The step can be passed as the checkbox will not be checked

    elif Marketing_consent == "ticked":
        # This checkbox unlike the previous two will be un-ticked. So, will assume this will need to clicked if the dataset asks for it to be ticked
        CorrectCheckBox = False
        # False boolean value used for indexed loop

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:8] != "I opt in":
                # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                n = n + 1
                if n == 3:
                    n = 0
                # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:8] == "I opt in":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    print(isChecked)
                    raise Exception("Marketing consent checkbox should not be automatically checked")
                    # If the response provides a true string value. An Exception is raised as this element should not be automatically checked.
                    # Due to GDPR we must ensure that the user has actively chosen to receive marking information

                elif isChecked == "false":
                    allCheckBoxes[n].click()
                    # if the response provides a false string value. The element is clicked to check the element


@step("Create Account button is clicked")
def step_impl(context):
    scroll_down_to_element("fragment_registration__button_signup")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    createAccountBtn = driver.find_element_by_id("fragment_registration__button_register")
    createAccountBtn.click()
    # The create account button is then found by its resource ID and is then clicked


@step('Terms and Conditions field is "(?P<T_C_tickbox>.+)"')
def step_impl(context, T_C_tickbox):

    if T_C_tickbox == "ticked":
        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        try:

            CorrectCheckBox = False
            # False boolean value used for indexed loop

            driver.swipe(start_x=20, start_y=800, end_x=20, end_y=500)
            # Touch action used to ensure all of the fields are visible to be interacted with

            n = 0
            # Sets "n" to "0" to be used as an identifier for the index value

            allCheckBoxes = driver.find_elements_by_id("checkbox_field")
            # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

            while not CorrectCheckBox:
                if allCheckBoxes[n].text[0:11] != "I have read":
                    # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                    n = n + 1
                    if n == 2:
                        n = 0
                    # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

                elif allCheckBoxes[n].text[0:11] == "I have read":
                    CorrectCheckBox = True
                    # # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                    isChecked = allCheckBoxes[n].get_attribute('checked')
                    # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                    if isChecked == "true":
                        pass
                    # If the response provides a true string value. The element is automatically checked and needs no further action

                    elif isChecked == "false":
                        raise Exception("Terms and conditions checkbox was not automatically ticked")
                    # If the response provides a false string value. An exception is raised as this element should be automatically checked

        except:

            if driver.find_element_by_id("fragment_registration__textview_terms_conditions").text == "Data not available":
                raise Exception("Terms and conditions data did not load. Maybe the API is not working or the network connectivity is not good enough")
            # If an exception occurs here. It will be because the terms and conditions data had not loaded in as the terms and conditions text will show "Data not available"

            else:
                raise Exception("Terms and conditions checkboxes could not be located by their shared ID")
            # if an exception occurs here. The element could not be located by its ID. The elements ID may have changed

    elif T_C_tickbox == "not ticked":
        # If the dataset has this data set as "not ticked" the relevant check box must be clicked

        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        try:

            CorrectCheckBox = False
            # False boolean value used for indexed loop

            n = 0
            # Sets "n" to "0" to be used as an identifier for the index value

            allCheckBoxes = driver.find_elements_by_id("checkbox_field")
            # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

            while not CorrectCheckBox:
                if allCheckBoxes[n].text[0:11] != "I have read":
                    # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                    n = n + 1
                    if n == 2:
                        n = 0
                    # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

                elif allCheckBoxes[n].text[0:11] == "I have read":
                    CorrectCheckBox = True
                    # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                    isChecked = allCheckBoxes[n].get_attribute('checked')
                    # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                    if isChecked == "true":
                        allCheckBoxes[n].click()
                    # If the response provides a true string value. The element is clicked to ensure the status of checked is false

                    else:
                        raise Exception("Terms and conditions checkbox was not automatically ticked")
                    # If the response provides a false string value. An exception is raised as this element should be automatically checked

        except:

            if driver.find_element_by_id("fragment_registration__textview_terms_conditions").text == "Data not available":
                raise Exception("Terms and conditions data did not load. Maybe the API is not working or the network connectivity is not good enough")
            # If an exception occurs here. It will be because the terms and conditions data had not loaded in as the terms and conditions text will show "Data not available"

            else:
                raise Exception("Terms and conditions checkboxes could not be located by their shared ID")
            # if an exception occurs here. The element could not be located by its ID. The elements ID may have changed


@step('allowing Utilita to collect data field is "(?P<Data_consent>.+)"')
def step_impl(context, Data_consent):
    scroll_down_to_element("fragment_registration__button_signup")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    if Data_consent == "ticked":
        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        CorrectCheckBox = False
        # False boolean value used for loop that located the correct check box via an indexed loop

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:12] != "I understand":
                # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                n = n + 1
                if n == 2:
                    n = 0
                # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:12] == "I understand":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    pass
                # If the response provides a true string value. The element is automatically checked and needs no further action

                elif isChecked == "false":
                    raise Exception("The Data Consent checkbox was not automatically ticked")
                # If the response provides a false string value. An exception is raised as this element should be automatically checked

    elif Data_consent != "ticked":
        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        CorrectCheckBox = False
        # False boolean value used for loop that located the correct check box via an indexed loop

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:12] != "I understand":
                # If the text attribute of the element is not matched with this hard coded text. Then this path is followed until this is matched

                n = n + 1
                if n == 2:
                    n = 0
                # Increases the index value by 1 and attempts to match the text. If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:12] == "I understand":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute. True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox. The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    allCheckBoxes[n].click()
                # If the response provides a true string value. The element is clicked to ensure the checked attribute becomes false

                elif isChecked == "false":
                    raise Exception("The Data Consent checkbox was not automatically ticked")
                # If the response provides a false string value. An exception is raised as this element should be automatically checked


@step("user clicks 'Already Registered' button")
def step_impl(context):
    scroll_down_to_element("fragment_registration__button_signup")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    alreadySignBtn = driver.find_element_by_id("fragment_registration__button_signup")
    alreadySignBtn.click()
    # Finds the already registered element by its resource ID and clicks this


@then("Sign in screen is showing")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    signInWindow = driver.find_element_by_id("fragment_login__imageview_logo")
    signInWindow.is_displayed()
    # Waits for the sign in page to show and locates the Utilita logo to ensure the correct page has loaded


@step("user clicks 'View the Legal Stuff'")
def step_impl(context):
    scroll_down_to_element("fragment_registration__checkbox_terms_expander")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    checkboxExpander = driver.find_element_by_id("fragment_registration__checkbox_terms_expander")
    checkboxExpander.click()
    # Finds the toggle expander is clicked which reveals the legal stuff retrieved from the API call


@then("Home screen shows")
def step_impl(context):
    WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Waits for the next page to load by waiting to detect the presence of the element to be located

    signInHomeWindow = driver.find_element_by_id("fragment_login__imageview_logo")
    signInHomeWindow.is_displayed()
    # Sign in window shows after registration. Before the user would be taken to home screen page


@then('"(?P<Response>.+)" Shows next to the relevant field impacted')
def step_impl(context, Response):

    if Response == "Please enter your name":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_fullname")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        fullNameErrorField = driver.find_element_by_id("fragment_registration__validation_fullname")
        fullNameErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "That email address doesnt seem right":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_email")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        emailErrorField = driver.find_element_by_id("fragment_registration__validation_email")
        emailErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "This password doesn't seem right":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_password")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        passWordErrorField = driver.find_element_by_id("fragment_registration__validation_password")
        passWordErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "Please enter your Customer Reference Number Customer Reference Number should be 10 numbers":
        # Checks which response path should be followed
        
        scroll_up_to_element("fragment_registration__validation_crn")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        custRefErrorField = driver.find_element_by_id("fragment_registration__validation_crn")
        custRefErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "Please select a date of birth":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_dob")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        dOBErrorField = driver.find_element_by_id("fragment_registration__validation_dob")
        dOBErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "That postcode doesn't seem right":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_postcode")
        # Scroll up Function is called from the Function.py file that can be found in the steps folder

        noPostCodeErrorField = driver.find_element_by_id("fragment_registration__validation_postcode")
        noPostCodeErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "TC Consent Required":
        # Checks which response path should be followed

        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        CorrectCheckBox = False
        # False boolean value used for indexed loop

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:11] != "I have read":
                # If the text attribute of the element is not matched with this hard coded text.
                # Then this path is followed until this is matched

                n = n + 1
                if n == 2:
                    n = 0
                    # Increases the index value by 1 and attempts to match the text.
                    # If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:11] == "I have read":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute.
                # True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox.
                # The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    raise Exception("Terms and conditions acceptance checkbox was not un-ticked")
                # If the response provides a true string value.
                # An Exception is raised as the checkbox was not un-checked

                elif isChecked == "false":
                    pass
                # If the response provides a false string value.
                # The step can be passed as the checkbox was un-checked

    elif Response == "DC Consent Required":
        # Checks which response path should be followed

        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        CorrectCheckBox = False
        # False boolean value used for loop that will locate the relevant error message via the text it is showing

        n = 0
        # Sets "n" to "0" to be used as an identifier for the index value

        allCheckBoxes = driver.find_elements_by_id("checkbox_field")
        # Creates a list of elements that share the same ID as there is more than 1 check box with this ID

        while not CorrectCheckBox:
            if allCheckBoxes[n].text[0:12] != "I understand":
                # If the text attribute of the element is not matched with this hard coded text.
                # Then this path is followed until this is matched

                n = n + 1
                if n == 2:
                    n = 0
                    # Increases the index value by 1 and attempts to match the text.
                    # If the index becomes 3 it is set back to 0 as there are only a few check boxes

            elif allCheckBoxes[n].text[0:12] == "I understand":
                CorrectCheckBox = True
                # Correct checkbox is located via matching the elements text attribute.
                # True boolean value used to close the loop

                isChecked = allCheckBoxes[n].get_attribute('checked')
                # Using the index value of the desired checkbox.
                # The checked attribute can be obtained. This provides a boolean value

                if isChecked == "true":
                    raise Exception("Data consent acceptance checkbox was not un-ticked")
                # If the response provides a true string value.
                # An Exception is raised as the checkbox was not un-checked

                elif isChecked == "false":
                    pass
                # If the response provides a false string value.
                # The step can be passed as the checkbox was un-checked

    elif Response == "Min 8 characters, must have a upper and lowercase letters and a number":
        # Checks which response path should be followed

        scroll_up_to_element("fragment_registration__validation_password")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        problemWithPasswordErrorField = driver.find_element_by_id("fragment_registration__validation_password")
        problemWithPasswordErrorField.is_displayed()
        # Locates the exact error message element and checks that it is displayed

    elif Response == "The details you entered do not match the information we hold for you.":
        # Checks which response path should be followed

        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folde

        x = 0
        # Sets "x" as zero to be used as an identifier for the index value in the following loop

        CorrectError = False
        # False boolean value used for loop that will locate the relevant error message via the text it is showing

        while not CorrectError:
            detailsEnteredDoNotMatch = driver.find_elements_by_class_name("android.widget.TextView")
            errorPromptText = detailsEnteredDoNotMatch
            # Due to this message not having a resource ID. I had to locate all elements in the class and compare the text shown

            numOfTextEle = detailsEnteredDoNotMatch.__len__()
            # Defines integer value of the total elements sharing this class name on the screen

            if errorPromptText[x].text[0:68] != "The details you entered do not match the information we hold for you":
                x = x + 1
                # If the text is not matched the index value is increased by 1

                if x == numOfTextEle:
                    raise Exception("Error stating that the data entered does not match with the account could not be found")
                # If the text can not be found and the index value reaches the max count of elements.
                # An exception error will be produced explaining what the issue may be

            elif errorPromptText[x].text[0:68] == "The details you entered do not match the information we hold for you":
                detailsEnteredDoNotMatch[x].is_displayed()
                CorrectError = True
                # Once the error text is matched here. It checks that this element is displayed on the page

    elif Response == "Please select a valid date of birth":
        # Checks which response path should be followed

        scroll_down_to_element("fragment_registration__button_signup")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        createAccountBtn = driver.find_element_by_id("fragment_registration__button_signup")
        createAccountBtn.is_displayed()
        # Locates the exact error message element and checks that it is displayed

        x = 0
        # Sets "x" as zero to be used as an identifier for the index value in the following loop

        CorrectError = False
        # False boolean value used for loop that will locate the relevant error message via the text it is showing

        while not CorrectError:
            detailsEnteredDoNotMatch = driver.find_elements_by_class_name("android.widget.TextView")
            errorPromptText = detailsEnteredDoNotMatch
            # Due to this message not having a resource ID. I had to locate all elements in the class and compare the text shown

            numOfTextEle = detailsEnteredDoNotMatch.__len__()
            # Defines integer value of the total elements sharing this class name on the screen

            if errorPromptText[x].text[0:68] != "The details you entered do not match the information we hold for you":
                x = x + 1
                # If the text is not matched the index value is increased by 1

                if x == numOfTextEle:
                    raise Exception("Invalid date of birth error text could not be found")
                # If the text can not be found and the index value reaches the max count of elements.
                # An exception error will be produced explaining what the issue may be

            elif errorPromptText[x].text[0:68] == "The details you entered do not match the information we hold for you":
                CorrectError = True
                detailsEnteredDoNotMatch[x].is_displayed()
                # Once the error text is matched here. It checks that this element is displayed on the page


@then("T&C details shows above legal dialogue/ accordion screen")
def step_impl(context):
    scroll_down_to_element("fragment_registration__textview_terms_conditions")
    # Scroll down Function is called from the Function.py file that can be found in the steps folde

    TC_Content = driver.find_element_by_id("fragment_registration__textview_terms_conditions")
    if TC_Content.text != "Data not available":
        pass
    # If the text attribute of this element does not equal "No Data Available". Then the full terms and conditions are displayed

    elif TC_Content.text == "Data not available":
        raise Exception("Terms and conditions data did not load. Maybe the API is not working or the network connectivity is not good enough")
    # If the text attribute of this element does equal "No Data Available". An exception is raised


@step("Legal stuff appears in dialogue/ accordion screen")
def step_impl(context):
    scroll_down_to_element("fragment_registration__textview_terms_expanded")
    # Scroll down Function is called from the Function.py file that can be found in the steps folde

    termsRevealed = driver.find_element_by_id("fragment_registration__textview_terms_expanded")
    termsRevealed.is_displayed()
    # Checks this element is displayed on the screen via the elements resource ID
