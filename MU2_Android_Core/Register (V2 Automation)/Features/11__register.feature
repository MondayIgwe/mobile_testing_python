@JIRA-APP-120 @JIRA-APP-121 @JIRA-APP-122 @JIRA-APP-123
Feature: 1.1 - Register
    As a non register user

    In order to access the app and have an account

    I want to become a registered user

  @done @test
  Scenario Outline: User successfully register for Utilita app
    Given register user screen shows
    And name field is filled with "<Full_name>"
    And email field is filled with "<Email>"
    And mobile number field is filled with "<Mobile>"
    And password field is filled with "<Password>"
    And Customer reference field is filled with "<Customer_reference_number>"
    And date of birth field is filled with "<DoB>"
    And postcode is filled with "<Postcode>"
    And Terms and Conditions field is ticked
    And allowing Utilita to collect data field is ticked
    And Marketing field is "<Marketing_consent>"
    And Create Account button is clicked
    Then Home screen shows

    Examples:
      | Full_name    | Email                 | Mobile      | Password  | Customer_reference_number | DoB        | Postcode | Marketing_consent |
      | Tester McGee | testing@utilita.co.uk | 07123456789 | Testing01 | 1307139158                | 01/11/1999 | SO53 3QB | ticked            |

  @done
  Scenario Outline: Registration failed filled data not pass validation
    Given register user screen shows
    And name field is filled with "<Full_name>"
    And email field is filled with "<Email>"
    And mobile number field is filled with "<Mobile>"
    And password field is filled with "<Password>"
    And Customer reference field is filled with "<Customer_reference_number>"
    And date of birth field is filled with "<DoB>"
    And postcode is filled with "<Postcode>"
    And Terms and Conditions field is "<T_C_tickbox>"
    And allowing Utilita to collect data field is "<Data_consent>"
    And Create Account button is clicked
    Then "<Response>" Shows next to the relevant field impacted

    Examples:
      | Full_name    | Email                 | Mobile      | Password  | Customer_reference_number | DoB           | Postcode | T_C_tickbox | Data_consent | Response                      |
      | Tester McGee | testing@utilita.co.uk | 07123456789 | Testing01 | 1307139158                | (leave blank) | SO53 3QB | ticked      | ticked       | Please select a date of birth |

  @done
  Scenario: Redirect to user login page
    Given register user screen shows
    And user clicks 'Already Registered' button
    Then Sign in screen is showing

  @done
  Scenario: Read Terms and Conditions
    Given register user screen shows
    And user clicks 'View the Legal Stuff'
    Then T&C details shows above legal dialogue/ accordion screen
    And Legal stuff appears in dialogue/ accordion screen
