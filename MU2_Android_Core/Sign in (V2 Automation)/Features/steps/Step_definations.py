from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.steps.Function import scroll_down_to_element

from Features.Env import driver

use_step_matcher("re")


@step("MyUtilita has loaded")
def step_impl(context):
    WebDriverWait(driver, 6).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    driver.find_element_by_id("fragment_login__imageview_logo").is_displayed()
    # Checks window is loaded by looking for logo element.

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

    except:
        if email.is_displayed():
            pass
        # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@step("Sign in screen is showing")
def step_impl(context):
    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    driver.set_network_connection(4)
    # Turns on data connections on the device.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

    except:
        if email.is_displayed():
            pass
        # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@step('add "(?P<Email>.+)" to email field')
def step_impl(context, Email):
    email = driver.find_element_by_id("fragment_login__edittext_email")
    email.send_keys(Email)
    driver.hide_keyboard()
    # Inputs email address from example data set into email field via the command for sending keys.


@step('add "(?P<Password>.+)" to password field')
def step_impl(context, Password):
    if Password == "null":
        pass
    # If the password is null in the dataset. Then this step is ignored.

    else:
        password_field = driver.find_element_by_id("fragment_login__edittext_password")
        password_field.send_keys(Password)
        driver.hide_keyboard()
    # If the password is not null in the dataset. Sends this data through via sending keys then hides keyboard.


@step("Sign in button is clicked")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_login__loadingbutton_signin")))
    signInButton = driver.find_element_by_id("fragment_login__loadingbutton_signin")
    signInButton.click()
    # Finds and Clicks sign in button. Keyboard may not be closed. Driver is told to implicitly wait for this.


@step("Home screen shows")
def step_impl(context):
    try:

        HomePageLoaded = False
        while not HomePageLoaded:
            if WebDriverWait(driver, 30).until(EC.presence_of_element_located((By.ID, "text_address"))):
                HomePageLoaded = True
                # Tries to wait until the home page loads. Checks if the address element is displayed.

            elif driver.find_element_by_id("retry").is_displayed():
                # Loop starts. attempts to locate the no connection error message via the element's resource ID.

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Retry element is located by its resource ID. Element is then clicked and the driver is instructed to wait in case the message reappears..

    except(TimeoutError, Exception):
        raise Exception("Home page did not load within 30 seconds of signing in and the retry call button did not show")
        # If the home page does not load an except clause attempts to look for the connection error and retry button.


@step('validation message shows next to "(?P<Error_area>.+)"')
def step_impl(context, Error_area):
    if Error_area == "Incorrect password":
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "text_password_validation_confirm")))
        errorMessage = driver.find_element_by_id("text_password_validation_confirm")
        errorMessage.is_displayed()
    # If the Error_area is incorrect password. It looks for this element when its displayed after sign in fails.

    elif Error_area == "Incorrect email":
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "fragment_login__textview_email_validation")))
        errorMessage = driver.find_element_by_id("fragment_login__textview_email_validation")
        errorMessage.is_displayed()
    # If the Error_area is incorrect email. It looks for this element when its displayed after failed sign in attempt.


@step("user clicks 'Forgot your password' link")
def step_impl(context):
    scroll_down_to_element("fragment_login__textview_forgot_password")
    # Scroll down Function is called from the Function.py file that can be found in the steps folde

    passwordButton = driver.find_element_by_id("fragment_login__textview_forgot_password")
    passwordButton.click()
    # Finds forgotten password link and clicks this.


@then("forgotten password screen appears")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "text_title")))
    forgotPassTitle = driver.find_element_by_id("text_title")
    assert forgotPassTitle.text == "Forgot password"
    # Tries to find the text title of this page via the resource ID and asserts the title of forgotten password.


@step("user clicks create account link")
def step_impl(context):
    scroll_down_to_element("fragment_login__textview_create_account")
    # Scroll down Function is called from the Function.py file that can be found in the steps folde

    createAccountButton = driver.find_element_by_id("fragment_login__textview_create_account")
    createAccountButton.click()
    # Finds create account button by the resource ID and clicks this element.


@then("register user screen shows")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_registration__inputfield_fullname")))
    regInputField = driver.find_element_by_id("fragment_registration__inputfield_fullname")
    regInputField.is_displayed()
    # Finds the full name input element via the resource ID and checks that this element is displayed.


@then('message is shown \' "(?P<Error_message>.+)" \' underneath sign in button in red')
def step_impl(context, Error_message):
    if Error_message == "Incorrect email and password":
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "fragment_login__textview_email_validation")))
        emailValidationText = driver.find_element_by_id("fragment_login__textview_email_validation")
        emailValidationText.is_displayed()
    # If the Error_message is "Incorrect email and password". It checks that the relevant error is displayed.

    elif Error_message == "Incorrect password":
        passwordValidationText = driver.find_elements_by_class_name("android.widget.TextView")
        numTextView = passwordValidationText.__len__()
        x = 0
    # The Incorrect password error does not have a resource ID. A list of elements with this class_name is built.

        if passwordValidationText[x].text != "Invalid credentials please try again":
            x = x + 1
            # if the text in an element within this list is not matched.
            # The index value is increased by 1

            if x == numTextView:
                raise Exception("Password error element could not be located on the sign in page")
                # if the text in an element within this list is not matched
                # And the total amount of elements has been checked. An Exception error is raised

        elif passwordValidationText[x].text == "Invalid credentials please try again":
            pass
    # If the text is matched. Then the element was found and is displaying the necessary text. Step is then Passed.

    elif Error_message == "Incorrect email":
        WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "fragment_login__textview_email_validation")))
        emailValidationText = driver.find_element_by_id("fragment_login__textview_email_validation")
        emailValidationText.is_displayed()
    # If the Error_message is Incorrect email. The relevant error is then found and checked that it is displayed.


@step("there is no connection")
def step_impl(context):
    driver.set_network_connection(0)
    # Turns all network connections off for next step.


@then('alert message "Login Failed sorry no connection" is shown')
def step_impl(context):
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "errorText")))
    noConnectionError = driver.find_element_by_id("errorText")
    noConnectionError.is_displayed()
    # Finds the error displayed when there is no connection.
