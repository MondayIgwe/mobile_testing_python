@JIRA-APP-118 @JIRA-APP-119
Feature: 1.0 - Sign in
    Sign in as a user to use the app

  @done
  Scenario Outline: User successfully sign in for Utilita app
    Given MyUtilita has loaded
    And Sign in screen is showing
    When add "<Email>" to email field
    And add "<Password>" to password field
    And Sign in button is clicked
    Then Home screen shows

    Examples:
      | Email                     | Password  |
      | testaddress@utilita.co.uk | Testtest1 |

  @done
  Scenario Outline: User failed to sign in for Utilita app - validation
    Given MyUtilita has loaded
    And Sign in screen is showing
    When add "<Email>" to email field
    And add "<Password>" to password field
    And Sign in button is clicked
    Then validation message shows next to "<Error_area>"

    Examples:
      | Email                     | Password  | Error_area         |
      | testaddress@utilita.co.uk |  null     | Incorrect Password |
      | wrongemail                | Testtest1 | Incorrect email    |

  @done
  Scenario Outline: User failed to sign in for Utilita app - incorrect login
    Given MyUtilita has loaded
    And Sign in screen is showing
    And add "<Email>" to email field
    And add "<Password>" to password field
    And Sign in button is clicked
    Then message is shown ' "<Error_message>" ' underneath sign in button in red

    Examples:
      | Email                     | Password       | Error_message                |
      | wrong email               | wrong password | Incorrect email and password |
      | testaddress@utilita.co.uk | wrong password | Incorrect password           |
      | wrong email               | Testtest1      | Incorrect email              |

  @done
  Scenario Outline: User failed to sign in for Utilita app - no connection
    Given MyUtilita has loaded
    And Sign in screen is showing
    And there is no connection
    Then add "<Email>" to email field
    And add "<Password>" to password field
    And Sign in button is clicked
    Then alert message "Login Failed sorry no connection" is shown

    Examples:
      | Email                     | Password  |
      | testaddress@utilita.co.uk | Testtest1 |

  @done
  Scenario: Password recovery - link
    Given MyUtilita has loaded
    And Sign in screen is showing
    And user clicks 'Forgot your password' link
    Then forgotten password screen appears

  @done
  Scenario: Create account for new user
    Given Sign in screen is showing
    And user clicks create account link
    Then register user screen shows
