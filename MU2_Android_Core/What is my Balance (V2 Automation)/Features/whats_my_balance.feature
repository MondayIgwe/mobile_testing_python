Feature: What's my balance

  @done
  Scenario Outline: What's my balance
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    And User's current estimated balance will be visible

    Examples:
      | Email              | Password  |
      | testing25@test.com | Testtest1 |
  @done
  Scenario Outline: How long until my supply balance is low
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    And Estimated time left is visible

    Examples:
      | Email              | Password  |
      | testing25@test.com | Testtest1 |

  @NeedsFixToApp @MUA-660 @done
  Scenario Outline: I wish to use my Payment card barcode via the app
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    When User selects the payment card barcode expander
    Then Payment card bardcode will be revealed

    Examples:
      | Email              | Password  |
      | testing25@test.com | Testtest1 |
  @done
  Scenario Outline: What's my Payment card number
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    And Payment card number will be displayed on the customers energy card

    Examples:
      | Email              | Password  |
      | testing25@test.com | Testtest1 |
  @done
  Scenario Outline: I wish to see my debt and recovery rate values in the app
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    Given User still has debt
    When User views their energy cards
    Then Remaining debt and recovery rates will be displayed

    Examples:
      | Email              | Password  |
      | testing10@test.com | Testtest1 |
  @done
  Scenario Outline: I would like more information about my debt
    Given My Utilita is loaded
    When User signs into the app with "<Email>"
    And Uses the password "<Password>"
    Then Home Screen shows
    Given User still has debt
    When User views their energy cards
    Then Remaining debt and recovery rates will be displayed
    When User selects the Recovery rate CTA button
    Then Recovery rate details page will become visible

    Examples:
      | Email              | Password  |
      | testing10@test.com | Testtest1 |
