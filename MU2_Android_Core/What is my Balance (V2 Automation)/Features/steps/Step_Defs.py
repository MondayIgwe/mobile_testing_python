from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.steps.Function import waiting_for_home_screen


from Features.Env import driver

use_step_matcher("re")


@given("My Utilita is loaded")
def step_impl(context):
    WebDriverWait(driver, 6).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Explicitly waits for the logo element of the sign in page to be located via it's resource ID

    driver.find_element_by_id("fragment_login__imageview_logo").is_displayed()
    # Locates the log in logo via its resource ID. Once located, a check is made to ensure the element is displayed

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

    except:
        if email.is_displayed():
            pass
        # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception("Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@when('User signs into the app with "(?P<Email>.+)"')
def step_impl(context, Email):
    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Locates the email field element via the elements resource ID

    email.click()
    WebDriverWait(driver, 5).until_not(EC.staleness_of(email))
    emailNotStale = driver.find_element_by_id("fragment_login__edittext_email")
    # A stale element error was occurring when the send keys command was used.
    # To handle this. The driver is explicitly instructed to wait for the element to no longer be stale

    emailNotStale.send_keys(Email)
    driver.hide_keyboard()
    # Implicitly waits for the field to be ready for interaction.
    # The email address from the dataset example will be inserted into this element via the send keys command.
    # Mobile Keyboard is then hidden


@step('Uses the password "(?P<Password>.+)"')
def step_impl(context, Password):
    passwordField = driver.find_element_by_id("fragment_login__edittext_password")
    # Locates the password field element via the elements resource ID.

    passwordField.send_keys(Password)
    driver.hide_keyboard()
    # The password from the dataset example will be inserted into this element via the send keys command.
    # Mobile Keyboard is then hidden

    driver.implicitly_wait(5)
    signInButton = driver.find_element_by_id("fragment_login__loadingbutton_signin")
    signInButton.click()
    # Keyboard may not be closed. Driver is told to implicitly wait for this element to be intractable
    # Finds and Clicks sign in button.

    try:

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
        # Tries to explicitly wait for the home navigation button to become visible

    except:
        try:

            signInError = driver.find_element_by_id("errorText")
            signInError.is_displayed()
            # Tries to locate the error message that appears when the sign in attempt fails.
            # Once located, aa check is made to ensure the element is currently visible

            signInButton.click()
            # Locates the sign in button element and clicks this to retry the sign in attempt

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
            # Explicitly waits for the home navigation button element to become visible via the elements resource ID

        except:
            raise Exception("Sign in attempt failed. One retry attempt also failed")
            # An Exception error is raised if the sign in attempts fail. As the scenario can not be completed


@then("Home Screen shows")
def step_impl(context):
    waiting_for_home_screen()
    # Calls function stored in the Function.py file located in the steps folder to ensure the home page has loaded


@step("User's current estimated balance will be visible")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "balance_label")))
    labelBalance = driver.find_element_by_id("balance_label")
    labelBalance.is_displayed()
    # Locates the balance label element via the elements resource ID.
    # Once located, a check is made to ensure the element is visible.

    currentBalance = driver.find_element_by_id("balance")
    currentBalance.is_displayed()
    # Locates the balance element via the elements resource ID.
    # Once located, a check is made to ensure the element is visible.
    # As this elements text value changes too often. An assertion of this string will not work.


@step("Estimated time left is visible")
def step_impl(context):
    driver.swipe(start_x=10, start_y=1550, end_x=10, end_y=900)
    # A swipe action is used to scroll down the page to ensure the desired element is visible

    WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "minutes_ago_chronometer")))
    # Explicitly waits for Updated just now element to be visible via locating the elements resource ID

    timeUpdated = driver.find_element_by_id("minutes_ago_chronometer")
    timeUpdated.is_displayed()
    # Locates chronometer element via the elements resource ID.
    # Once located, a check is made to ensure the element is visible


@when("User selects the payment card barcode expander")
def step_impl(context):
    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "barcode")))
    # Explicitly waits for the barcode element to be located via the elements resource ID

    try:

        barcodeExpander = driver.find_element_by_id("barcode")
        barcodeExpander.click()
        # Locates barcode section expander element via the elements resource ID.
        # Once located, the element is clicked

        driver.find_element_by_id("img_barcode")
        # Tries to locate the barcode image in case the issue described in MUA-660 has been resolved

    except:
        try:

            barcodeExpander = driver.find_element_by_id("barcode")
            barcodeExpander.click()
            driver.find_element_by_id("img_barcode")
            # If the issue described in MUA-660 has not been resolved. The barcode section expander element is clicked
            # Now that the element was made focusable in the first click. The second will expand the barcode image
            # Locates the barcode Image via the elements resource ID

        except:
            raise Exception("Barcode image was not located. Barcode Expander may not have revealed the section - "
                            "Known issue - Please refer to MUA-660")
        # An exception will be raised if there is a problem with locating the barcode image or expanding this section


@then("Payment card bardcode will be revealed")
def step_impl(context):
    barcodeImage = driver.find_element_by_id("img_barcode")
    barcodeImage.is_displayed()
    # Locates the barcode Image via the elements resource ID
    # Once located, a check is made to ensure the element is visible


@step("Payment card number will be displayed on the customers energy card")
def step_impl(context):
    try:
        payCardNumber = driver.find_element_by_id("topup_number")
        payCardNumber.is_displayed()
        # Locates payment card number element via the elements resource ID.
        # Once located, a check is made to ensure the element is visible

        assert payCardNumber.text[0:7] == "9826 00"
        # An assertion of this elements text attribute I made to ensure the string matches the desired text

    except:
        try:
            payCardNumber = driver.find_element_by_id("topup_number")
            payCardNumber.is_displayed()
            # Locates payment card number element via the elements resource ID.
            # Once located, a check is made to ensure the element is visible

            assert payCardNumber.text[0:7] == "9826 00"
            # An assertion of this elements text attribute I made to ensure the string matches the desired text

        except:
            raise Exception("Payment card number could not be found on the energy supply cards")


@given("User still has debt")
def step_impl(context):
    try:
        
        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "help_cta")))
        # Explicitly waits for the more information CTA button to be located via the elements resource ID

        recRateHelp = driver.find_element_by_id("help_cta")
        recRateHelp.is_displayed()
        # Locates the recovery rate help element via the elements resource ID.
        # Once located, a check is made to ensure the element is visible

    except:
        raise Exception("Customer no longer has debt. If a data refresh has occurred. A different account will need to be used")
        # Raises an Exception if the recovery rate help element can not be located or is not visible


@when("User views their energy cards")
def step_impl(context):
    energyCard = driver.find_element_by_id("layout_data")
    energyCard.is_displayed()
    # Locates Energy card element via the elements resource ID
    # Once located, a check is made to ensure the element is visible

    energyIcon = driver.find_element_by_id("icon")
    energyIcon.is_displayed()
    # Locates the energy icon element via the elements resource ID
    # Once located, a check is made to ensure the element is visible


@then("Remaining debt and recovery rates will be displayed")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "pill_value")))
    # Explicitly waits for the recovery and debt elements to be located via the elements shared resource ID

    x = 0
    # x is used here as an identifier for the index value used to determine the current element in focus

    ChargesFound = False
    # False boolean value used for the following loop

    while not ChargesFound:
        # Loop will determine whether the Pound symbol shows as the value is likely to change after a data refresh

        valuesOnCards = driver.find_elements_by_id("pill_value")
        # Locates elements that share this resource ID. Once located, the elements are added to a list array

        if valuesOnCards[x].text[0:1] != "£":
            # The text attribute of the element in focus is reduced to only contain the Pound symbol

            x = x + 1
            # If the elements text attribute is not matched to the desired text
            # The index value is increased by 1 to change the element in focus

        elif valuesOnCards[x].text[0:1] == "£":
            # The text attribute of the element in focus is reduced to only contain the Pound symbol
            # If the elements shortened text attribute matches the desired text. The correct element has be located

            ChargesFound = True
            # True boolean value used to close the loop now that the correct element has been located

            valuesOnCards[x].is_displayed()
            # A check is made to ensure the element is visible

            try:

                RecoveryFound = False
                # False boolean value used for the following loop

                n = 0
                # x is used here as an identifier for the index value used to determine the current element in focus

                while not RecoveryFound:
                    # Loop will determine whether the percentage symbol shows.
                    # As the value is likely to change after a data refresh

                    percent0 = valuesOnCards[n].text.replace("0", "")
                    percent1 = percent0.replace("1", "")
                    percent2 = percent1.replace("2", "")
                    percent3 = percent2.replace("3", "")
                    percent4 = percent3.replace("4", "")
                    percent5 = percent4.replace("5", "")
                    percent6 = percent5.replace("6", "")
                    percent7 = percent6.replace("7", "")
                    percent8 = percent7.replace("8", "")
                    percentFinal = percent8.replace("9", "")
                    # Replaces all of the numbers in the string with nothing so only the percentage is in the string

                    if percentFinal != "%":
                        n = n + 1
                        # If the shortened string does not equal the desired text.
                        # The index value is increased by one to change the element that is in focus

                    elif percentFinal == "%":
                        # If the elements shortened text attribute matches the desired text.
                        # The correct element has be located

                        RecoveryFound = True
                        # True boolean value used to close the loop now that the correct element has been located

                        valuesOnCards[n].is_displayed()
                        # Once the correct element has been located.
                        # A check is made to ensure the element is visible

            except:
                raise Exception("Other charges element found. Recovery rate element could not be located")
                # An exception is raised here if the percentage is missing from the recovery rate string

        else:
            raise Exception("Other charges element could not be located")
            # An exception is raised here if the pound symbol is missing from the other charges string


@when("User selects the Recovery rate CTA button")
def step_impl(context):
    recRateCTA = driver.find_element_by_id("help_cta")
    recRateCTA.click()
    # Locates recovery rate help element via the elements resource ID
    # Once located, the element is clicked


@then("Recovery rate details page will become visible")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "title")))
    # Explicitly waits for the title element to be located via the elements resource ID

    recRateTitle = driver.find_element_by_id("title")
    recRateTitle.is_displayed()
    # Locates the recovery rate title element via the elements resource ID
    # Once located, a check is made to ensure the element is visible

    recRateAmount = driver.find_element_by_id("recovery_rate")
    recRateAmount.is_displayed()
    # Locates the recovery rate element via the elements resource ID
    # Once located, a check is made to ensure the element is visible

    weCollect = driver.find_element_by_id("we_collect_amount")
    weCollect.is_displayed()
    # Locates the recovery rate element via the elements resource ID
    # Once located, a check is made to ensure the element is visible

    youTopUp = driver.find_element_by_id("you_top_up")
    youTopUp.is_displayed()
    # Locates the recovery rate element via the elements resource ID
    # Once located, a check is made to ensure the element is visible
