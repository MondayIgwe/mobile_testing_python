from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Env import driver
import datetime
from Features.steps.Function import scroll_down_to_element

use_step_matcher("re")


@given("My Utilita is loaded")
def step_impl(context):
    WebDriverWait(driver, 6).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Explicitly waits for the logo element of the sign in page to be located via it's resource ID

    driver.find_element_by_id("fragment_login__imageview_logo").is_displayed()
    # Locates the log in logo via its resource ID. Once located, a check is made to ensure the element is displayed

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

    except:
        if email.is_displayed():
            pass
        # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@step("Customer is Dual Fuel")
def step_impl(context):
    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Locates the email field element via the elements resource ID

    email.click()
    WebDriverWait(driver, 5).until_not(EC.staleness_of(email))
    emailNotStale = driver.find_element_by_id("fragment_login__edittext_email")
    # A stale element error was occurring when the send keys command was used.
    # To handle this. The driver is explicitly instructed to wait for the element to no longer be stale

    emailNotStale.send_keys("testaddress@utilita.co.uk")
    driver.hide_keyboard()
    # Implicitly waits for the field to be ready for interaction.
    # The email address from the dataset example will be inserted into this element via the send keys command.
    # Mobile Keyboard is then hidden

    passwordField = driver.find_element_by_id("fragment_login__edittext_password")
    # Locates the password field element via the elements resource ID.

    passwordField.send_keys("Testtest1")
    driver.hide_keyboard()
    # The password from the dataset example will be inserted into this element via the send keys command.
    # Mobile Keyboard is then hidden

    driver.implicitly_wait(5)
    signInButton = driver.find_element_by_id("fragment_login__loadingbutton_signin")
    signInButton.click()
    # Keyboard may not be closed. Driver is told to implicitly wait for this element to be intractable
    # Finds and Clicks sign in button.

    try:

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
        # Tries to explicitly wait for the home navigation button to become visible

    except:
        try:

            signInError = driver.find_element_by_id("errorText")
            signInError.is_displayed()
            # Tries to locate the error message that appears when the sign in attempt fails.
            # Once located, aa check is made to ensure the element is currently visible

            signInButton.click()
            # Locates the sign in button element and clicks this to retry the sign in attempt

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
            # Explicitly waits for the home navigation button element to become visible via the elements resource ID

        except:
            raise Exception("Sign in attempt failed. One retry attempt also failed")
            # An Exception error is raised if the sign in attempts fail. As the scenario can not be completed


@step("User is already logged in")
def step_impl(context):
    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
        # Explicitly waits for the Retry button element to be located via it's resource ID

        retryBtn = driver.find_element_by_id("retry")
        retryBtn.click()
        # Locates Retry button element via the elements resource ID. Once located, the element is clicked

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
        # Explicitly waits for the balance label element to be located via it's resource ID

    except:
        try:

            for i in range(10):
                WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Locates Retry button element via the elements resource ID.
                # Once located, the element is clicked

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
            # Explicitly waits for the balance label element to be located via it's resource ID

        except:
            try:

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Locates Retry button element via the elements resource ID. Once located, the element is clicked

                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
                # Explicitly waits for the balance label element to be located via it's resource ID

            except:
                try:

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
                    # Explicitly waits for the balance label element to be located via it's resource ID

                except:
                    try:

                        addressBar = driver.find_element_by_id("text_address")
                        addressBar.is_displayed()
                        # Tries to determine if the home before check has completed.
                        # Locates the address element shown at the top of the home page via its resource ID
                        # Once located, a check is made to ensure the element is visible

                        paymentNavBtn = driver.find_element_by_id("payments")
                        paymentNavBtn.click()
                        # Locates the payments Navigation button element via its resource ID
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                        # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                        homeNavBtn = driver.find_element_by_id("home")
                        homeNavBtn.click()
                        # Locates the home Navigation button element via its resource ID
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
                        # Explicitly waits for the balance label element to be located via it's resource ID

                    except:
                        try:

                            for u in range(10):
                                WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                # The driver will be explicitly instructed to wait until the progress bar is hidden

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID. Once located, the element is clicked

                            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
                            # Explicitly waits for the balance label element to be located via it's resource ID

                        except:
                            try:

                                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "supplier_stack")))
                                # Explicitly waits for the balance label element to be located via it's resource ID

                            except(TimeoutError, Exception):
                                raise Exception(
                                    "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                # If the home page does not load an except clause attempts to look for the connection error and retry button.


@when("User selects Payments Navigation button")
def step_impl(context):
    paymentNavBtn = driver.find_element_by_id("payments")
    paymentNavBtn.click()
    # Payments navigation element is located via its resource ID. Once located, the element is clicked

    try:
        # Try will attempt to determine if the correct page is visible after selecting the payment navigation button

        x = 0
        # x will be used as an identifier for indexed element in focus

        paymentText = driver.find_elements_by_class_name("android.widget.TextView")
        # Locates all elements that share this class name and creates a list array

        CorrectPage = False
        # Boolean value set to False that will be used in the following loop

        while not CorrectPage:
            if paymentText[x].text == "Make a payment":
                CorrectPage = True
                pass
                # If the text attribute of the current element in focus matches the desired text. The loop can end

            elif paymentText[x].text != "Make a payment":
                x = x + 1
                # If the text attribute of the current element in focus does not match the desired text
                # The x value is increased by 1 so another element in the list becomes the focus

    except:

        paymentNavBtn.click()
        # If an element that carries the class name can not be located or the index limit is exceeded.
        # The correct page is most likely not visible. The payments navigation button is then clicked


@then("Make a Payment button becomes visible")
def step_impl(context):
    oldField = "layout_make_a_payment"
    fragmentID = "fragment_payments_make_a_payment_option"
    # ID of the make a payment element that will be used below in this step definition

    try:

        WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, fragmentID)))
        currentFieldID = fragmentID
        # This try statement will attempt to determine if the old ID is being used in the current build in the app

        try:

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Driver waits for the retry button element to be visible
            # Once the driver locates this element via its resource ID. The retry button is then clicked

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.is_displayed()
            # Make a payment element is then located by its resource ID.
            # Then a check is made to determine if the element is displayed

        except:

            paymentsNavBtn = driver.find_element_by_id("payments")
            paymentsNavBtn.click()
            # Payments Navigation element is located by its resource ID. The element is then clicked

            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, currentFieldID)))
            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.is_displayed()
            # Driver then waits for the make a payment element to become visible by locating the element via its resource ID
            # Then a check is made to determine if the element is displayed

    except:

        currentFieldID = oldField
        # If try fails when locating the make a payment button by its old ID. Then it will be using the new fragment ID

        try:

            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "retry")))
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Driver waits for the retry button element to be visible
            # Once the driver locates this element via its resource ID. The retry button is then clicked

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.is_displayed()
            # Make a payment element is then located by its resource ID.
            # Then a check is made to determine if the element is displayed

        except:

            paymentsNavBtn = driver.find_element_by_id("payments")
            paymentsNavBtn.click()
            # Payments Navigation element is located by its resource ID. The element is then clicked

            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, currentFieldID)))
            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.is_displayed()
            # Driver then waits for the make a payment element to become visible by locating the element via its resource ID
            # Then a check is made to determine if the element is displayed


@when("User select make a payment")
def step_impl(context):
    oldField = "layout_make_a_payment"
    fragmentID = "fragment_payments_make_a_payment_option"
    # ID of the make a payment element that will be used below in this step definition

    try:

        driver.find_element_by_id("fragment_payments_make_a_payment_option")
        currentFieldID = fragmentID
        # This try statement will attempt to determine if the old ID is being used in the current build in the app

        try:

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Driver waits for the retry button element to be visible
            # Once the driver locates this element via its resource ID. The retry button is then clicked

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.click()
            # Make a payment element is then located by its resource ID.
            # The element is then clicked

        except:

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.click()
            # Make a payment element is then located by its resource ID.
            # The element is then clicked

    except:

        currentFieldID = oldField
        # If try fails when locating the make a payment button by its old ID. Then it will be using the new fragment ID

        try:

            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "retry")))
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Driver waits for the retry button element to be visible
            # Once the driver locates this element via its resource ID. The retry button is then clicked

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.click()
            # Make a payment element is then located by its resource ID.
            # The element is then clicked

        except:

            makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
            makeAPaymentBtn.click()
            # Make a payment element is then located by its resource ID.
            # The element is then clicked


@then("Make a Payment - Select page becomes visible")
def step_impl(context):
    oldFieldID = "layout_make_a_payment"
    fragmentID = "fragment_payments_make_a_payment_option"
    # ID of the make a payment element that will be used below in this step definition

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
        # Driver tries to wait for an element shown in the Payment - Select page to become visible by located its ID

    except:
        try:

            WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "retry")))
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Tries to locate the retry button element if the supply's payment card number is not visible

            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
            # Driver waits for the supply payment card number to become visible

        except:
            try:

                mu2Web = driver.find_element_by_id("com.android.chrome:id/image")
                mu2Web.is_displayed()
                # Locates chrome logo element via its resource ID in case the chrome setup page has become visible
                # A check is then made to determine if the page is visible

                WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "send_report_checkbox")))
                # Explicitly waits for the send report checkbox to be locatable via the elements resource ID

                sendReport = driver.find_element_by_id("send_report_checkbox")
                sendReport.click()
                # Locates the send usage report element via the elements resource ID
                # The element is then clicked

                acceptBtn = driver.find_element_by_id("terms_accept")
                acceptBtn.click()
                # Locates the accept and close button element via the elements resource ID
                # The element is then clicked

                noThanksBtn = driver.find_element_by_id("negative_button")
                noThanksBtn.click()
                # Locates the No Thanks button element via the elements resource ID
                # The element is then clicked

                driver.back()
                # Now that the chrome setup is complete. The back button will return the device to My Utilita app

                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                # Driver waits for the supply payment card number to become visible

            except:
                try:

                    driver.find_element_by_id("com.android.chrome:id/url_bar")
                    driver.back()
                    # If the chrome terms were accepted in a previous scenario. The Utilita Web page will appear
                    # The driver is then instructed to return to the previous screen via the devices back button

                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                    # Driver waits for the supply payment card number to become visible

                except:
                    try:

                        retryBtn = driver.find_element_by_id("retry")
                        retryBtn.click()
                        # Tries to locate the retry button element if the supply's payment card number is not visible

                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                        # Driver waits for the supply payment card number to become visible

                    except:
                        try:

                            driver.find_element_by_id("home").click()
                            # If the supply's payment card number is not visible. The driver returns to home to reload the app

                            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "retry")))
                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Driver is instructed to wait for the retry button to become visible.
                            # Once located by its resource ID. The element is clicked

                            driver.find_element_by_id("payments").click()
                            # Driver locates the payments navigation element and clicks the element

                            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "retry")))
                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Driver is instructed to wait for the retry button to become visible.
                            # Once located by its resource ID. The element is clicked

                            try:

                                driver.find_element_by_id(fragmentID)
                                currentFieldID = fragmentID
                                # This try statement will attempt to determine if the old ID is being used in the current build in the app

                                makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                makeAPaymentBtn.click()
                                # Make a payment element is then located by its resource ID.
                                # The element is then clicked

                                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                # Driver waits for the supply payment card number to become visible

                            except:

                                currentFieldID = oldFieldID
                                # If try fails when locating the make a payment button by its old ID. Then it will be using the new fragment ID

                                makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                makeAPaymentBtn.click()
                                # Make a payment element is then located by its resource ID.
                                # The element is then clicked

                                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                # Driver waits for the supply payment card number to become visible

                        except:
                            try:

                                driver.find_element_by_id("payments").click()
                                # Driver locates the payments navigation element and clicks the element

                                WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "retry")))
                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Driver is instructed to wait for the retry button to become visible.
                                # Once located by its resource ID. The element is clicked

                                try:

                                    driver.find_element_by_id(fragmentID)
                                    currentFieldID = fragmentID
                                    # This try statement will attempt to determine if the old ID is being used in the current build in the app

                                    makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                    makeAPaymentBtn.click()
                                    # Make a payment element is then located by its resource ID.
                                    # The element is then clicked

                                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                    # Driver waits for the supply payment card number to become visible

                                except:

                                    currentFieldID = oldFieldID
                                    # If try fails when locating the make a payment button by its old ID. Then it will be using the new fragment ID

                                    makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                    makeAPaymentBtn.click()
                                    # Make a payment element is then located by its resource ID.
                                    # The element is then clicked

                                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                    # Driver waits for the supply payment card number to become visible

                            except:
                                try:

                                    driver.find_element_by_id(fragmentID)
                                    currentFieldID = fragmentID
                                    # This try statement will attempt to determine if the old ID is being used in the current build in the app

                                    try:

                                        makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                        makeAPaymentBtn.click()
                                        # Make a payment element is then located by its resource ID.
                                        # The element is then clicked

                                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                        # Driver waits for the supply payment card number to become visible

                                    except:
                                        try:

                                            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                            # Driver waits for the supply payment card number to become visible

                                        except:
                                            raise Exception("Payment - Select page did not become visible. Known issue. Please refer to MUA-653 and MUA-656")
                                            # If the Payment card number element can not be located
                                            # An Exception error is raised as the handling of this problem did not work

                                except:

                                    currentFieldID = oldFieldID
                                    # If try fails when locating the make a payment button by its old ID. Then it will be using the new fragment ID

                                    try:

                                        makeAPaymentBtn = driver.find_element_by_id(currentFieldID)
                                        makeAPaymentBtn.click()
                                        # Make a payment element is then located by its resource ID.
                                        # The element is then clicked

                                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                        # Driver waits for the supply payment card number to become visible

                                    except:
                                        try:

                                            WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "text_supply_pan")))
                                            # Driver waits for the supply payment card number to become visible

                                        except:
                                            raise Exception("Payment - Select page did not become visible. Known issue. Please refer to MUA-653 and MUA-656")
                                            # If the Payment card number element can not be located
                                            # An Exception error is raised as the handling of this problem did not work


@when('User selects £"(?P<First_Amount>.+)" for one service that is visible')
def step_impl(context, First_Amount):

    if First_Amount == "0":
        driver.swipe(start_x=20, start_y=1000, end_x=20, end_y=100)
        pass
        # If first amount to be inputted is 0.
        # Swipe action is used to scroll down the screen so the next step can begin

    elif First_Amount != "0":
        scroll_down_to_element("textedit_amount")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        chooseAmount = driver.find_element_by_id("textedit_amount")
        chooseAmount.send_keys(First_Amount)
        driver.hide_keyboard()
        # Once the field is visible.
        # The amount stored in the test data is passed through to the field via the send keys command and the keyboard is closed


@step('User selects £"(?P<Second_Amount>.+)" for the other visible service that is visible')
def step_impl(context, Second_Amount):

    if Second_Amount == "0":
        driver.swipe(start_x=20, start_y=1000, end_x=20, end_y=100)
        pass
        # If second amount to be inputted is 0.
        # Swipe action is used to scroll down the screen so the next step can begin

    if Second_Amount != "0":
        scroll_down_to_element("checkbox_confimation_email")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        chooseAmount = driver.find_element_by_id("textedit_amount")
        chooseAmount.send_keys(Second_Amount)
        driver.hide_keyboard()
        # Now that the email checkbox has been located. Only the second edit amount field will be visible
        # The edit amount element is then located by its resource ID.
        # The amount value from the data set is passed through to the field via a send keys command and the keyboard is closed

        driver.swipe(start_x=20, start_y=1000, end_x=20, end_y=100)
        # Swipe action is then used to ensure the email checkbox is visible enough for a future step


@then('Total Basket Amount becomes £"(?P<Total_Amount>.+)"')
def step_impl(context, Total_Amount):
    totalAmount = driver.find_element_by_id("text_total").text
    trueTotal = totalAmount.replace("£", "")
    assert trueTotal == Total_Amount
    # Total amount element is located via its resource ID. The text is then stored as a string
    # The Pound sign in the string is replaced with nothing so the value can be matched with the value in the dataset


@step("Email receipt option should already be checked")
def step_impl(context):
    emailRec = driver.find_element_by_id("checkbox_confimation_email")
    isChecked = emailRec.get_attribute('checked')
    # Email receipt checkbox is then located via its resource ID. Once located the checked status is obtained
    # This status will be received as a string of "true" or "false" and not as a boolean value

    if isChecked == "true":
        pass
        # This checkbox should be automatically checked. If the status equals "true". This step can be passed

    elif isChecked == "false":
        raise Exception("Email receipt option was not automatically checked")
        # If the checkbox is not automatically checked and provides a "false" value. An Exception is raised


@when("User selects the Next button")
def step_impl(context):
    nextBtn = driver.find_element_by_id("button_cta_next")
    nextBtn.click()
    # Next button is located by its resource ID. Once located, the element is clicked

    try:

        if WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry"))):
            retryBtn = driver.find_element_by_id("retry")
            retryBtn.click()
            # Will try to determine if there was a network issue in the emulator.
            # If the retry button is located, it will be clicked

            visatitle = driver.find_element_by_id("visa_checkout_title")
            visatitle.is_displayed()
            # visa checkout title element is located via the elements resource ID.
            # Once located, a check is made to ensure the element is displayed

            try:

                if WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry"))):
                    retryBtn = driver.find_element_by_id("retry")
                    retryBtn.click()
                    # Will try to determine if there was a network issue in the emulator.
                    # If the retry button is located, it will be clicked

                    visatitle = driver.find_element_by_id("visa_checkout_title")
                    visatitle.is_displayed()
                    # visa checkout title element is located via the elements resource ID.
                    # Once located, a check is made to ensure the element is displayed

            except:
                try:

                    nextBtn.click()
                    # Once the retry button is gone. The next button is clicked

                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "visa_checkout_title")))
                    # Web Driver is explicitly instructed to wait until the visa checkout title can be located

                except:
                    raise Exception("Make a Payment - Payment page did not load correctly")
                    # if the make a Payment - payment page does not load. Then the script can not continue
                    # An Exception error is raised here

    except:

        if WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "visa_checkout_title"))):
            pass
            # If the retry or next button can not be located in the try statement.
            # WebDriver will wait for a text element to be located

        else:
            raise Exception("Make a Payment - Payment page did not load correctly")
            # if the make a Payment - payment page does not load. Then the script can not continue
            # An Exception error is raised here


@then("Make a Payment - Payment page becomes visible")
def step_impl(context):
    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "visa_checkout_title")))
        paymentTitle = driver.find_element_by_id("visa_checkout_title")
        paymentTitle.is_displayed()
        # Checks that this element is clearly displayed before moving to the next step

    except:
        raise Exception("Payments Payment page did not become visible")
        # An Exception is raised here if the Payment - Payment page does not become visible


@when("User selects pay with Debit/Credit card option")
def step_impl(context):
    scroll_down_to_element("button_pay_now")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    newCardExp = driver.find_element_by_id("section_save_new_card")
    newCardExp.click()
    # Locates the element via its resource ID. Once located, the element will be clicked


@then("Customer wishes to pay with a new card and the section is expanded")
def step_impl(context):
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "visa_description")))
    newCardDes = driver.find_element_by_id("visa_description")
    assert newCardDes.text == "Pay with a Debit/Credit Card"
    # Locates the element via its resource ID. Once located. The elements text attribute is matched to the desired text


@when('User inputs "(?P<Card_Number>.+)"')
def step_impl(context, Card_Number):
    if len(Card_Number) == 16:
        # Determines which field should be interacted with by counting the amount of characters in the string currently in focus

        scroll_down_to_element("text_card_number_input")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        cardNumInput = driver.find_element_by_id("text_card_number_input")
        cardNumInput.send_keys(Card_Number)
        driver.hide_keyboard()
        # Card number element is located by its ID.
        # Once located, the Card number value from the dataset is inputted via the send keys command and the keyboard is closed

    elif len(Card_Number) == 15:
        # Determines which field should be interacted with by counting the amount of characters in the string currently in focus

        scroll_down_to_element("text_card_number_input")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        cardNumInput = driver.find_element_by_id("text_card_number_input")
        cardNumInput.send_keys(Card_Number)
        driver.hide_keyboard()
        # Card number element is located by its ID.
        # Once located, the Card number value from the dataset is inputted via the send keys command and the keyboard is closed

    elif len(Card_Number) == 5:
        # Determines which field should be interacted with by counting the amount of characters in the string currently in focus

        scroll_down_to_element("input_expiration_month")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        monthExpiry = driver.find_element_by_id("input_expiration_month")
        monthExpiry.send_keys(Card_Number[0:2])
        driver.hide_keyboard()
        # Locates the Expiration month field.
        # Once located the shortened month value is inputted via the send keys command and the keyboard is closed

        yearExpiry = driver.find_element_by_id("input_expiration_year")
        yearExpiry.send_keys(Card_Number[3:5])
        driver.hide_keyboard()
        # Locates the Expiration year field.
        # Once located the shortened year value is inputted via the send keys command and the keyboard is closed

    elif len(Card_Number) == 3:
        # Determines which field should be interacted with by counting the amount of characters in the string currently in focus

        scroll_down_to_element("input_expiration_month")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        cvvField = driver.find_element_by_id("input_cvv_field")
        cvvField.send_keys(Card_Number)
        driver.hide_keyboard()
        # Element is located by its resource ID.
        # Once located, the CVV value is inputted via the send keys command and the keyboard is closed


@step("User Selects Pay Now")
def step_impl(context):
    scroll_down_to_element("button_pay_now")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    payNowBtn = driver.find_element_by_id("button_pay_now")
    payNowBtn.click()
    # The pay now element is then located via its resource ID.
    # Once located, the element is clicked


@then("Payment will be taken")
def step_impl(context):
    pass
    # This step can not be automated as it is different on every scenario. Everything that could be covered is handled in other steps


@step("Make a Payment - Summary page becomes visible")
def step_impl(context):
    try:

        WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.ID, "summary_body_message")))
        # Driver is explicitly instructed to wait until the summary text element can be located

        summaryTitle = driver.find_element_by_id("text_title")
        assert summaryTitle.text == "Summary"
        # Summary title element is located by its ID.
        # Once located, the elements text attribute is matched to the desired text

        summaryBody = driver.find_element_by_id("summary_body_message")
        assert summaryBody.text[0:30] == "Your payment has been accepted"
        # Summary body element is then located by its resource ID.
        # Once located, the elements text attribute is matched to the desired text

    except:
        try:

            buttonClass = driver.find_elements_by_class_name("android.widget.Button")
            # Elements that share this class name are located and put into a list array

            x = 0
            # x will be used as an identifier for the indexed element in focus

            RightButton = False
            # False boolean value used for the following loop that will locate the finish button in the visa checkout

            while not RightButton:
                if buttonClass[x].text != "FINISH SETUP & PAY":
                    x = x + 1
                    # If the text attribute of the indexed element in focus does not match the desired text
                    # The index value is increased by 1 to change the element in focus

                elif buttonClass[x].text == "FINISH SETUP & PAY":
                    raise Exception("Visa checkout has not closed and the error lies with their SDK")
                    # If the text attribute of the element in focus is matched. Then the visa checkout window has not closed
                    # An Exception will be raised here as the payment could not be completed

        except:
            raise Exception("Visa checkout environment was closed. However. the My Utilita payment summary page did not become visible. If crash occurred on enter a new card scenario. Please check status of MUA-700")
            # If the summary page did not become visible and the visa checkout page is no longer visible
            # An Exception will be raised as there is an issue with completing the payment in the app


@when("User Chooses to pay with Saved card")
def step_impl(context):
    scroll_down_to_element("button_pay_now")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    try:

        savedCard = driver.find_element_by_id("vw_selection")
        savedCard.is_displayed()
        # Locates the saved card element via its resource ID.
        # Once located, a check is made to ensure the element is displayed

        WebDriverWait(driver, 15).until(EC.visibility_of_element_located((By.ID, "iv_payment_method_card_type_default")))
        # Driver is instructed to explicitly wait until the element is displayed via locating the elements resource ID

    except:
        raise Exception("Saved Card did not load into Payment - Payment page. Possible network issue")
        # If the saved card elements can not be located.
        # An exception error is raised to state that the saved card displayed.
        # This occurs occasionally in the debug environment


@then("CVV confirmation prompt becomes visible")
def step_impl(context):
    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "confirm_cvv_dialog_component_container")))
    # Driver is explicitly instructed to wait until the this element can be located via its resource ID

    cvvPrompt = driver.find_element_by_id("confirm_cvv_dialog_component_input_cvv_field")
    cvvPrompt.is_displayed()
    # CVV field element is located via its resource ID.
    # Once located, a check is made to ensure the element is displayed


@when('User inputs "(?P<CVV>.+)" into Confirmation prompt field')
def step_impl(context, CVV):
    cvvPrompt = driver.find_element_by_id("confirm_cvv_dialog_component_input_cvv_field")
    cvvPrompt.send_keys(CVV)
    driver.hide_keyboard()
    # CVV field element is located via its resource ID.
    # CVV value from the dataset is inputted into this field via the send keys command and the keyboard is then closed


@step("User selects Pay Now on Confirmation prompt")
def step_impl(context):
    payNowBtn = driver.find_element_by_id("confirm_cvv_dialog_component_positive")
    payNowBtn.click()
    # Pay Now button element is located via its resource ID.
    # Once located, the element is then clicked


@when("User chooses to pay with Visa Checkout")
def step_impl(context):
    pass
    # This step context is handled in other step definitions


@step("User selects the Visa Checkout - Click to Pay option")
def step_impl(context):
    try:

        visaCheckout = driver.find_element_by_id("visa_checkout")
        visaCheckout.click()
        # Tries to locate the visa checkout element via its resource ID.
        # Once located, the element is then clicked

    except:
        raise Exception("Known Error - Visa Checkout / Click to Pay option not enabled in current app build")
        # If the visa checkout element can not be located. It may not have been included in the latest build
        # As this can be displayed via the database config table
        # Or the element may still be hidden in certain branches since it was reintroduced
        # However and exception error will be raised here as this scenario can not be completed


@then("Visa Checkout payment environment becomes visible")
def step_impl(context):
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
    # Driver is instructed to wait until elements sharing a certain class name can be be located


@when('User inputs "(?P<Card_Number>.+)" into Checkout fields')
def step_impl(context, Card_Number):
    cardFields = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name and creates a list array of these elements

    if len(Card_Number) == 16:
        # Determines which field needs to be selected by counting the characters in the string obtained from the dataset

        cardFields[0].send_keys(Card_Number)
        driver.hide_keyboard()
        # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
        # The card number element is the first element in the list and is located with the index value of 0
        # Once located, the card number value from the dataset is inputted via the send keys command and keyboard is then closed

    elif len(Card_Number) == 15:
        # Determines which field needs to be selected by counting the characters in the string obtained from the dataset

        cardFields[0].send_keys(Card_Number)
        driver.hide_keyboard()
        # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
        # The card number element is the first element in the list and is located with the index value of 0
        # Once located, the card number value from the dataset is inputted via the send keys command and keyboard is then closed

    elif len(Card_Number) == 5:
        # Determines which field needs to be selected by counting the characters in the string obtained from the dataset

        expireDate = Card_Number.replace("/", "")
        cardFields[1].send_keys(expireDate)
        driver.hide_keyboard()
        # Expiry data from the dataset is reduced so it only contains numbers by using .replace to remove the slash
        # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
        # The Expiry date element is the second element in the list and is located with the index value of 1
        # Once located, the Expiry value from the dataset is inputted via the send keys command and keyboard is then closed

    elif len(Card_Number) == 3:
        # Determines which field needs to be selected by counting the characters in the string obtained from the dataset

        cardFields[2].send_keys(Card_Number)
        driver.hide_keyboard()
        # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
        # The CVV element is the third element in the list and is located with the index value of 2
        # Once located, the CVV value from the dataset is inputted via the send keys command and keyboard is then closed


@step("User selects Continue")
def step_impl(context):
    ScreenClear = False
    # False boolean value used for the following loop

    m = 0
    # m is used as an identifier for the index value in the loop below

    acceptBtn = driver.find_elements_by_class_name("android.widget.Button")
    # Locates element that share this class name. Once located, a list array is created. Containing these elements

    while not ScreenClear:
        try:

            if acceptBtn[m].text != "ACCEPT":
                m = m + 1
                # If the text attribute of the element in focus can not be matched. The index value is increase by 1

            elif acceptBtn[m].text == "ACCEPT":
                acceptBtn[m].click()
                # If the text attribute of the element in focus can be matched with the desired text
                # Once located, the element in focus is clicked

                ScreenClear = True
                # True boolean value used here to end the loop

                buttonClass = driver.find_elements_by_class_name("android.widget.Button")
                # Locates elements that share this class name. Once located, a list array is created containing these elements

                RightButton = False
                # False boolean value used for the following loop

                x = 0
                # x is used here as an identifier for the index value

                while not RightButton:
                    if buttonClass[x].text != "CONTINUE":
                        x = x + 1
                        # If the text attribute of the element in focus does not match the desired text.
                        # The index value is increased by 1

                    elif buttonClass[x].text == "CONTINUE":
                        buttonClass[x].click()
                        # If the text attribute of the element in focus does match the desired text
                        # The element in focus is then clicked

                        RightButton = True
                        # True boolean value used to complete the loop

                        WebDriverWait(driver, 20).until_not(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
                        # Driver is instructed to wait explicitly until an element with this class name can be located

        except:
            try:

                ScreenClear = True
                # True boolean value used to close the loop above before starting the next loop

                buttonClass = driver.find_elements_by_class_name("android.widget.Button")
                # Locates elements that share this class name. Once located, the elements are added to a list array

                RightButton = False
                # False boolean value used for the following loop

                n = 0
                # n is used here as an identifier for the index value

                while not RightButton:
                    if buttonClass[n].text != "CONTINUE":
                        n = n + 1
                        # If the text attribute of the element in focus can not be matched to the desired text
                        # The index value is increased by 1 so the element in focus will be changed

                    elif buttonClass[n].text == "CONTINUE":
                        buttonClass[n].click()
                        # If the text attribute of the element in focus matches the desired text.
                        # The element in focus will be clicked

                        RightButton = True
                        # True boolean value used to close the loop now that the correct element has been located

                        WebDriverWait(driver, 20).until_not(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
                        # Driver is instructed to explicitly wait until an element with this class name can be located

                        WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
                        # Driver is instructed to explicitly wait until an element with this class name can be located

            except:

                WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
                # Driver is instructed to explicitly wait until an element with this class name can be located


@then("Billing information page becomes visible")
def step_impl(context):
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.EditText")))
    # Driver is instructed to explicitly wait until an element with this class name can be located


@when('User inputs "(?P<First_Name>.+)" into First Name field')
def step_impl(context, First_Name):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[0].send_keys(First_Name)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The First Name element is the first element in the list and is located with the index value of 0
    # Once located, the First Name value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Second_Name>.+)" into Second Name')
def step_impl(context, Second_Name):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[1].send_keys(Second_Name)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The Second Name element is the second element in the list and is located with the index value of 1
    # Once located, the Second Name value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Address>.+)" into the Address 1 field')
def step_impl(context, Address):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[3].send_keys(Address)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The address element is the fourth element in the list and is located with the index value of 3
    # Once located, the address value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Town>.+)" into the Town field')
def step_impl(context, Town):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[5].send_keys(Town)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The Town element is the Sixth element in the list and is located with the index value of 5
    # Once located, the Town value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Postcode>.+)" into the Postcode field')
def step_impl(context, Postcode):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[6].send_keys(Postcode)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The Postcode element is the Seventh element in the list and is located with the index value of 6
    # Once located, the Postcode value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Phone_Number>.+)" into the Phone Number field')
def step_impl(context, Phone_Number):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    buttonClass[7].send_keys(Phone_Number)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The Phone Number element is the eighth element in the list and is located with the index value of 7
    # Once located, the Phone Number value from the dataset is inputted via the send keys command and keyboard is then closed


@step('User inputs "(?P<Email>.+)" into the Email field')
def step_impl(context, Email):
    buttonClass = driver.find_elements_by_class_name("android.widget.EditText")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    t = datetime.datetime.now()
    ti = str(t)
    ti = ti.replace(" ", "")
    ti = ti.replace("-", "")
    ti = ti.replace(":", "")
    ti = ti.replace(".", "")
    ti = ti[0:12]
    si1 = (ti + Email)
    # Takes the current time and turns the timestamp into a string.
    # Any spaces, hyphens, Colons and semi colons are removed using .replace
    # The time stamp is then reduce to minutes as anything small is unnecessary and is then added to the email value

    buttonClass[8].send_keys(si1)
    driver.hide_keyboard()
    # This element does not have a unique selector. Visa Checkout is also a SDK library that is not maintained by us
    # The Email element is the ninth element in the list and is located with the index value of 8
    # Once located, the Email value from the dataset is inputted via the send keys command and keyboard is then closed


@then("Review page shows")
def step_impl(context):
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.CheckBox")))
    # Driver is instructed to wait until an element with this class name can be located


@step("User selects Continue on Billing Information page")
def step_impl(context):
    driver.swipe(start_x=1000, start_y=1500, end_x=1000, end_y=450)
    # Swipe action used to scroll down the page so that the continue button becomes completely visible

    buttonClass = driver.find_elements_by_class_name("android.widget.Button")
    # Locates elements sharing this class name. Once located, the elements are added to list array

    RightButton = False
    # False boolean value used here for the following loop

    x = 0
    # x is used here as an identifier for the index value

    while not RightButton:
        if buttonClass[x].text[0:4] != "Next":
            x = x + 1
            # If the text attribute of the element in focus can not be matched to the desired text.
            # The index value is increased by 1 so the element in focus will change

        elif buttonClass[x].text[0:4] == "Next":
            buttonClass[x].click()
            # If the text attribute of the element in focus is matched to the desired text.
            # The element in focus is then clicked

            RightButton = True
            # True boolean value used to close the loop


@when("User selects the terms acknowledgement")
def step_impl(context):
    WebDriverWait(driver, 20).until(EC.presence_of_element_located((By.CLASS_NAME, "android.widget.CheckBox")))
    # Driver is instructed to explicitly wait until an element with this class name can be located

    checkBoxes = driver.find_elements_by_class_name("android.widget.CheckBox")
    # Locates elements that share this class name. Once located, they are added to a list array

    x = 0
    # x is used as an identifier for the index value

    RightCheckBox = False
    # False boolean value used for the following loop

    while not RightCheckBox:
        if checkBoxes[x].text[0:13] != "I acknowledge":
            x = x + 1
            # If the text attribute of the element in focus can not be matched to the desired text.
            # The index value is increased by 1 to change the element in focus

        elif checkBoxes[x].text[0:13] == "I acknowledge":
            checkBoxes[x].click()
            # If the text attribute of the element in focus can be matched to the desired text.
            # The element in focus is clicked

            RightCheckBox = True
            # True boolean value used to close the loop


@step("User selects Finish Setup")
def step_impl(context):
    driver.swipe(start_x=1000, start_y=1325, end_x=1000, end_y=450)
    # Swipe Action used to ensure the desired element is completely visible

    buttonClass = driver.find_elements_by_class_name("android.widget.Button")
    # Locates elements that share this class name. Once located, the elements are added to a list array

    x = 0
    # x is used here as an identifier for the index value

    RightButton = False
    # False boolean value used here for the following loop

    while not RightButton:
        if buttonClass[x].text != "FINISH SETUP & PAY":
            x = x + 1
            # If the text attribute of the element in focus can not be matched with the desired text.
            # The index value is increased by 1 to change the element in focus

        elif buttonClass[x].text == "FINISH SETUP & PAY":
            buttonClass[x].click()
            # If the text attribute of the element in focus can be matched to the desired text.
            # The element in focus is clicked

            RightButton = True
            # True boolean value used to close the loop
