Feature: Payment Automation


  @JIRA-TT-490 @JIRA-TTA-136 @done
  Scenario Outline: Making a successful payment With New Card - Core
    Given My Utilita is loaded
    And Customer is Dual Fuel
    And User is already logged in
    When User selects Payments Navigation button
    Then Make a Payment button becomes visible
    When User select make a payment
    Then Make a Payment - Select page becomes visible
    When User selects £"<First_Amount>" for one service that is visible
    And User selects £"<Second_Amount>" for the other visible service that is visible
    Then Total Basket Amount becomes £"<Total_Amount>"
    And Email receipt option should already be checked
    When User selects the Next button
    Then Make a Payment - Payment page becomes visible
    When User selects pay with Debit/Credit card option
    Then Customer wishes to pay with a new card and the section is expanded
    When User inputs "<Card_Number>"
    And User inputs "<Expiry_Date>"
    And User inputs "<CVV>"
    And User Selects Pay Now
    Then Payment will be taken
    And Make a Payment - Summary page becomes visible

    Examples:
      | First_Amount | Second_Amount | Total_Amount | Card_Number      | Expiry_Date | CVV |
      | 2            | 0             | 2.00         | 4111111111111111 | 02/25       | 444 |

  @JIRA-TT-490 @JIRA-TTA-136 @done
  Scenario Outline: Making a successful payment With Saved Card - Core
    Given My Utilita is loaded
    And Customer is Dual Fuel
    And User is already logged in
    When User selects Payments Navigation button
    Then Make a Payment button becomes visible
    When User select make a payment
    Then Make a Payment - Select page becomes visible
    When User selects £"<First_Amount>" for one service that is visible
    And User selects £"<Second_Amount>" for the other visible service that is visible
    Then Total Basket Amount becomes £"<Total_Amount>"
    And Email receipt option should already be checked
    When User selects the Next button
    Then Make a Payment - Payment page becomes visible
    When User Chooses to pay with Saved card
    And User Selects Pay Now
    Then CVV confirmation prompt becomes visible
    When User inputs "<CVV>" into Confirmation prompt field
    And User selects Pay Now on Confirmation prompt
    Then Payment will be taken
    And Make a Payment - Summary page becomes visible

    Examples:
      | First_Amount | Second_Amount | Total_Amount | CVV |
      | 2            | 0             | 2.00         | 444 |

  @JIRA-TT-490 @JIRA-TTA-136 @done
  Scenario Outline: Making a successful payment With Visa Checkout - Core
    Given My Utilita is loaded
    And Customer is Dual Fuel
    And User is already logged in
    When User selects Payments Navigation button
    Then Make a Payment button becomes visible
    When User select make a payment
    Then Make a Payment - Select page becomes visible
    When User selects £"<First_Amount>" for one service that is visible
    And User selects £"<Second_Amount>" for the other visible service that is visible
    Then Total Basket Amount becomes £"<Total_Amount>"
    And Email receipt option should already be checked
    When User selects the Next button
    Then Make a Payment - Payment page becomes visible
    When User chooses to pay with Visa Checkout
    And User selects the Visa Checkout - Click to Pay option
    Then Visa Checkout payment environment becomes visible
    When User inputs "<Card_Number>" into Checkout fields
    And User inputs "<Expiry_Date>" into Checkout fields
    And User inputs "<CVV>" into Checkout fields
    And User selects Continue
    Then Billing information page becomes visible
    When User inputs "<First_Name>" into First Name field
    And User inputs "<Second_Name>" into Second Name
    And User inputs "<Address>" into the Address 1 field
    And User inputs "<Town>" into the Town field
    And User inputs "<Postcode>" into the Postcode field
    And User inputs "<Phone_Number>" into the Phone Number field
    And User inputs "<Email>" into the Email field
    And User selects Continue on Billing Information page
    Then Review page shows
    When User selects the terms acknowledgement
    And User selects Finish Setup
    Then Payment will be taken
    And Make a Payment - Summary page becomes visible

    Examples:
      | First_Amount | Second_Amount | Total_Amount | Card_Number      | Expiry_Date | CVV | First_Name | Second_Name | Address   | Town        | Postcode | Phone_Number | Email                     | Password        |
      | 2            | 0             | 2.00         | 4111111111111111 | 02/25       | 444 | Tester     | McTesterton | Test Lane | Southampton | SO53 3QB | 07123456789  | testaddress@utilita.co.uk | Testtest1@2020! |
