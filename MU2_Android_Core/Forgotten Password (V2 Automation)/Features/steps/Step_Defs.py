from behave import *
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from Features.Env import driver
from Features.steps.Function import scroll_down_to_element


use_step_matcher("re")


@given("password recovery screen is showing")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Explicitly waits for the sign in logo image element to be located via the element's ID when it becomes visible

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

        scroll_down_to_element("fragment_login__textview_forgot_password")
        # Scroll down Function is called from the Function.py file that can be found in the steps folder

        forgotPass = driver.find_element_by_id("fragment_login__textview_forgot_password")
        forgotPass.click()
        # Locates the forgot password link element directly so the element can be clicked

        WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "text_title")))
        # Explicitly waits for the page title element is located via the elements resource ID

        forgotPassTitle = driver.find_element_by_id("text_title")
        assert forgotPassTitle.text == "Forgot password"
        # Locates the page title element via its resource ID.
        # The elements text attribute is then asserted to check that it matches the desired text

    except:
        if email.is_displayed():
            # If the email field is displayed then the rewards pop up did not show

            scroll_down_to_element("fragment_login__textview_forgot_password")
            # Scroll down Function is called from the Function.py file that can be found in the steps folder

            forgotPass = driver.find_element_by_id("fragment_login__textview_forgot_password")
            forgotPass.click()
            # Locates the forgot password link element directly so the element can be clicked

            WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "text_title")))
            # Explicitly waits for the page title element is located via the elements resource ID

            forgotPassTitle = driver.find_element_by_id("text_title")
            assert forgotPassTitle.text == "Forgot password"
            # Locates the page title element via its resource ID.
            # The elements text attribute is then asserted to check that it matches the desired text

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
            # Raises an exception if the email field and pop up message can not be located


@step('email field is filled with "(?P<Email>.+)"')
def step_impl(context, Email):
    emailInput = driver.find_element_by_id("input_email")
    emailInput.send_keys(Email)
    driver.hide_keyboard()
    # Locates email field element via the elements resource ID
    # The email contained in the dataset example is sent to this field via the send keys command
    # The Mobile Keyboard is then hidden


@step("user presses 'Reset Password' button")
def step_impl(context):
    forgotPassBtn = driver.find_element_by_id("button_submit")
    forgotPassBtn.click()
    # Locates Reset password submit button element via the elements resource ID.
    # Once located, the element is then clicked


@step("user clicks create account link")
def step_impl(context):
    createAccount = driver.find_element_by_id("text_create_account")
    createAccount.click()
    # The create account link element is located via its resource ID
    # Once located, the element is clicked


@then("register user screen shows")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_registration__inputfield_fullname")))
    # Explicitly waits the email input field on the registration page via the elements resource ID when it is visible


@given("My Utilita is loaded")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Explicitly waits the sign in logo element via the elements ID when it becomes visible

    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Email element is located via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

    except:
        if email.is_displayed():
            pass
        # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception(
                "Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


@when("User selects Forgotten Password link")
def step_impl(context):
    scroll_down_to_element("fragment_login__textview_forgot_password")
    # Scroll down Function is called from the Function.py file that can be found in the steps folder

    forgotPass = driver.find_element_by_id("fragment_login__textview_forgot_password")
    forgotPass.click()
    # Locates the forgot password link element directly so the element can be clicked


@then("Forgotten Password page becomes visible")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "text_title")))
    # Explicitly waits for the page title element is located via the elements resource ID

    forgotPassTitle = driver.find_element_by_id("text_title")
    assert forgotPassTitle.text == "Forgot password"
    # Locates the page title element via its resource ID.
    # The elements text attribute is then asserted to check that it matches the desired text


@when("User remembers their password and Selects Sign in page link")
def step_impl(context):
    signInBtn = driver.find_element_by_id("text_login")
    signInBtn.click()
    # The sign in button is located via the elements via the elements resource ID
    # Once located, the element is clicked


@then("Sign page becomes visible")
def step_impl(context):
    WebDriverWait(driver, 5).until(EC.presence_of_element_located((By.ID, "fragment_login__imageview_logo")))
    # Explicitly waits for the sign in logo to become visible via locating the element by its resource ID


@then(
    'the message: \'If the email you provided is valid, an email will be sent to "(?P<Email>.+)" with a link to reset your password\' is shown')
def step_impl(context, Email):
    WebDriverWait(driver, 5).until(EC.visibility_of_element_located((By.ID, "button_sign_in")))
    # Explicitly waits for the sign in button element to become visible via locating it by its resource ID

    resetConMsg = driver.find_elements_by_class_name("android.widget.TextView")
    # Creates list array of the elements that share this class name

    x = 0
    # x is used as an identifier for the index value used to determine the element in focus

    RightMessage = False
    # False boolean value used for the following loop below

    while not RightMessage:
        if resetConMsg[x].text[0:19] != "We've sent an email":
            x = x + 1
            # If the shortened text attribute of the element in focus is not matched with the desired text
            # The index value is increased by one to change the element in focus

        elif resetConMsg[x].text[0:19] == "We've sent an email":
            # If the shortened text attribute of the element in focus is matched to the desired text. The loop can end

            RightMessage = True
            # True boolean value is used here to end the loop now that the desired element has been located

            if resetConMsg[x].text[23:48] == Email:
                pass
                # If the shortened text attribute of the element in focus is matched with the email from the dataset example
                # This step can be passed

            else:
                raise Exception("Email address is not the same as the one previously entered")
                # An exception error is raised if the email address does not match with the email from the dataset example


@step("link back to Sign in screen shows")
def step_impl(context):
    signInBtn = driver.find_element_by_id("button_sign_in")
    signInBtn.is_displayed()
    # Sign In button element is located via the elements resource ID.
    # Once located, a check is made to ensure the element is displayed
