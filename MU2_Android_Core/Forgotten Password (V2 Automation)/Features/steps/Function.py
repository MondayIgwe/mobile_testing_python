from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait


from Features.Env import driver


def date_of_birth_widget(DoB: str):
    dayDate = DoB[0:2]
    monthDate = DoB[3:5]
    yearDate = DoB[6:10]
    # Separates the date three sections. So, the numbers for the day, month and year are separate

    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "android:id/date_picker_header_date")))
    # Waits for the date selection widget to appear by locating the element via its ID once it is visible on screen

    changeYearFeature = driver.find_element_by_id("android:id/date_picker_header_year")
    changeYearFeature.click()
    # Finds the year button on the date selection widget and clicks this to imitate the list of years

    WebDriverWait(driver, 3).until(EC.presence_of_element_located((By.ID, "android:id/text1")))
    # Ensures that the list of years is visible by locating on of these elements by its ID once the elements are visible

    rightYear = False
    # Creates a list of the elements shown in the list of years as they do not contain a unique ID.
    # False boolean value is set for the following loop

    n = 0
    # The letter "n" is used as an identifier for the indexed element the script is looking at.
    # It is set to "0" so it will start from the first element

    while not rightYear:
        selectYear = driver.find_elements_by_id("android:id/text1")
        if selectYear[n].text != yearDate:
            # Compares the indexed year in focus to the year date retrieved from the dataset.
            # If this is not matched it will increase the index value

            n = n + 1
            # Increases the indexed value by 1

            if n == 6:
                # This list of years only shows 6-7 years at 1 time. I have used 6 here to avoid a out of range error

                driver.swipe(start_x=600, start_y=700, end_x=600, end_y=1150)
                # A swipe action is used here to move down through the list so as to make more years visible

                n = 0
                # "n" is then also reset to "0" this is to allow the index function to repeat until the correct year is found

        else:
            rightYear = True
            selectYear[n].click()
            # Loop ends when a year in visible part of the list is matched with the year date from the data set

            monthDataTableNum = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"]
            months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
            # Both of these arrays are used for comparison with the text shown in the app and for checking the index this value has

            monthNumber = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]
            # Using the index value obtained with the arrays above. The script can select the right integer from this array

            dataTableMonth = False
            # False boolean value used for loop that determines the index value of the month in dataset based on the array above

            selectedDate = False
            # False boolean value used for loop used to determine index of month that is show in the app

            currentMonthSelected = driver.find_element_by_id("android:id/date_picker_header_date")
            currentMonthDate = currentMonthSelected.text[4:9]
            cd = currentMonthDate.strip()
            # Finds the text that displays the date selected and strips the text so only the month text is used for this comparison

            s = 0
            # Sets "s" as "0" to be used for the comparison of the month shown in the app against the array

            x = 0
            # Sets "x" as "0" to be used for the comparison of the month held in the dataset

            while not selectedDate:
                if months[x] != cd:
                    x = x + 1
                    # Based on the shown date and the months array.
                    # If the text is not matched. The index value is increased

                    if x == 11:
                        raise Exception("Check date picker text. Comma may have been added or removed.")
                        # Text shown in widget differs depending on the phone/android version

                else:
                    months[x] = cd
                    selectedDate = True
                    # Date displayed in the app and the month array are matched and the selectedDate loop ends but the "x" value is stored

                    while not dataTableMonth:
                        if monthDataTableNum[s] != monthDate:
                            s = s + 1
                            # Compares month number in the dataset to the month number string array.
                            # If this is not matched the index value is increase by 1

                        else:
                            dataTableMonth = True
                            # Month value is matched and dataTableMonth loop ends

                            requiredMonth = False
                            # False Boolean value set for use in creating a loop that selects the correct month

                            greaterMonth = monthNumber[x] > monthNumber[s]
                            greaterMonthDif = monthNumber[x] - monthNumber[s]
                            lessMonth = monthNumber[x] < monthNumber[s]
                            lessMonthDif = monthNumber[s] - monthNumber[x]
                            # "x" (date shown) is compared with "s" (date that needs to be selected) and determines whether "x" is greater or lesser than "s"

                            g = 0
                            # Sets "g" as "0". To be used in range loop below for increasing the selected month

                            i = 0
                            # Sets "i" as "0". To be used in range loop below for decreasing the selected month

                            if greaterMonth:
                                greaterMonthDif = monthNumber[x] - monthNumber[s]
                                # If the "x" value is larger than the "s" value. "s" is subtracted from "x" to get the correct difference value

                            elif lessMonth:
                                lessMonthDif = monthNumber[s] - monthNumber[x]
                                # If the "x" value is smaller than the "s" value. "x" is subtracted from "s" to get the correct difference value

                            while not requiredMonth:
                                if g in range(greaterMonthDif):
                                    changeMonthPrev = driver.find_element_by_id("android:id/prev")
                                    changeMonthPrev.click()
                                    # Locates and clicks previous button on the month selection tool

                                    g = g + 1
                                    # Increases the "g" value until it matches the greater difference as the month shown is greater than the month we wish to select

                                elif i in range(lessMonthDif):
                                    changeMonthNext = driver.find_element_by_id("android:id/next")
                                    changeMonthNext.click()
                                    # Locates and clicks the next button on month selection tool

                                    i = i + 1
                                    # Increase the "g" value until it matches the lesser difference as the month shown is a smaller value than the month we wish to select

                                else:
                                    requiredMonth = True
                                    # Month is now set correctly and the loop ends

                                    dayDateRequired = driver.find_elements_by_class_name("android.view.View")
                                    dayDateRequired[0].click()
                                    # A day is selected so the date shown is updated

                                    dayArray = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10",
                                                "11",
                                                "12", "13", "14", "15", "16", "17", "18", "19", "20", "21",
                                                "22",
                                                "23", "24", "25", "26", "27", "28", "29", "30", "31"]
                                    # Day date array is used to find the index number so the correct day is selected

                                    rightDay = False
                                    # False boolean value used for loop to select the right day on the calendar

                                    d = 0
                                    # "d" is used as an identifier of the indexed day

                                    while not rightDay:
                                        if dayArray[d] != dayDate:
                                            d = d + 1
                                            # If the day from the dataset is not matched with the first date in the array.
                                            # The "d" value is increased

                                            dayDateRequired[d].click()
                                            # Selects the correct day from the dataset via the indexed value

                                        elif dayArray[d] == dayDate:
                                            rightDay = True
                                            # Index is now correct matched with the string array above. Loop ends

                                            dOBSubmit = driver.find_element_by_id("android:id/button1")
                                            dOBSubmit.click()
                                            # Correct date is now selected.
                                            # The OK button is then located by its resource ID and clicked to complete the step.


def signing_in(EmailAddress: str, Password: str):
    email = driver.find_element_by_id("fragment_login__edittext_email")
    # Locates the email field element via the elements resource ID

    passwordField = driver.find_element_by_id("fragment_login__edittext_password")
    # Locates the password field element via the elements resource ID.

    signInBtn = driver.find_element_by_id("fragment_login__loadingbutton_signin")
    # Locates the Sign in button element via the elements resource ID.

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "collapse_button")))
        # Tries to wait until the pop up rewards message shows via locating the element by its resource ID

        collapseBtn = driver.find_element_by_id("collapse_button")
        collapseBtn.click()
        # Locates element via its resource ID. Once located, the element is clicked

        WebDriverWait(driver, 5).until_not(EC.staleness_of(email))
        emailNotStale = driver.find_element_by_id("fragment_login__edittext_email")
        # A stale element error was occurring when the send keys command was used.
        # To handle this. The driver is explicitly instructed to wait for the element to no longer be stale

        emailNotStale.send_keys(EmailAddress)
        driver.hide_keyboard()
        # Implicitly waits for the field to be ready for interaction.
        # The email address from the dataset example will be inserted into this element via the send keys command.
        # Mobile Keyboard is then hidden

        passwordField.send_keys(Password)
        # The password from the dataset example will be inserted into this element via the send keys command.
        # Mobile Keyboard is then hidden

        signInBtn.click()
        # Keyboard may not be closed. Driver is told to implicitly wait for this element to be intractable
        # Finds and Clicks sign in button.

        try:

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
            # Tries to explicitly wait for the home navigation button to become visible

            try:

                WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
                # Explicitly waits for the Retry button element to be located via it's resource ID

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Locates Retry button element via the elements resource ID.
                # Once located, the element is clicked

                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                # Explicitly waits for the supply card element to be located via it's resource ID

            except:
                try:

                    for i in range(10):
                        WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                        # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                        retryBtn = driver.find_element_by_id("retry")
                        retryBtn.click()
                        # Locates Retry button element via the elements resource ID.
                        # Once located, the element is clicked

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                    # Explicitly waits for the supply card element to be located via it's resource ID

                except:
                    try:

                        retryBtn = driver.find_element_by_id("retry")
                        retryBtn.click()
                        # Locates Retry button element via the elements resource ID.
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                        # Explicitly waits for the supply card element to be located via it's resource ID

                    except:
                        try:

                            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                            # Explicitly waits for the supply card element to be located via it's resource ID

                        except:
                            try:

                                addressBar = driver.find_element_by_id("text_address")
                                addressBar.is_displayed()
                                # Tries to determine if the home before check has completed.
                                # Locates the address element shown at the top of the home page via its resource ID
                                # Once located, a check is made to ensure the element is visible

                                paymentNavBtn = driver.find_element_by_id("payments")
                                paymentNavBtn.click()
                                # Locates the payments Navigation button element via its resource ID
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                                # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                                homeNavBtn = driver.find_element_by_id("home")
                                homeNavBtn.click()
                                # Locates the home Navigation button element via its resource ID
                                # Once located, the element is clicked

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                # Explicitly waits for the supply card element to be located via it's resource ID

                            except:
                                try:

                                    for u in range(10):
                                        WebDriverWait(driver, 5).until(
                                            EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                        # The driver will be explicitly instructed to wait until the progress bar is hidden

                                        retryBtn = driver.find_element_by_id("retry")
                                        retryBtn.click()
                                        # Locates Retry button element via the elements resource ID.
                                        # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.visibility_of_element_located((By.ID, "layout_data")))
                                    # Explicitly waits for the supply card element to be located via it's resource ID

                                except:
                                    try:

                                        WebDriverWait(driver, 10).until(
                                            EC.visibility_of_element_located((By.ID, "layout_data")))
                                        # Explicitly waits for the supply card element to be located via it's resource ID

                                    except(TimeoutError, Exception):
                                        raise Exception(
                                            "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                        # If the home page does not load an except clause attempts to look for the connection error and retry button.

        except:
            try:

                signInError = driver.find_element_by_id("errorText")
                signInError.is_displayed()
                # Tries to locate the error message that appears when the sign in attempt fails.
                # Once located, aa check is made to ensure the element is currently visible

                signInBtn.click()
                # Locates the sign in button element and clicks this to retry the sign in attempt

                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
                # Explicitly waits for the home navigation button element to become visible via the elements resource ID

                try:

                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
                    # Explicitly waits for the Retry button element to be located via it's resource ID

                    retryBtn = driver.find_element_by_id("retry")
                    retryBtn.click()
                    # Locates Retry button element via the elements resource ID.
                    # Once located, the element is clicked

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                    # Explicitly waits for the supply card element to be located via it's resource ID

                except:
                    try:

                        for i in range(10):
                            WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                            # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Locates Retry button element via the elements resource ID.
                            # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                        # Explicitly waits for the supply card element to be located via it's resource ID

                    except:
                        try:

                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Locates Retry button element via the elements resource ID.
                            # Once located, the element is clicked

                            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                            # Explicitly waits for the supply card element to be located via it's resource ID

                        except:
                            try:

                                WebDriverWait(driver, 10).until(
                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                # Explicitly waits for the supply card element to be located via it's resource ID

                            except:
                                try:

                                    addressBar = driver.find_element_by_id("text_address")
                                    addressBar.is_displayed()
                                    # Tries to determine if the home before check has completed.
                                    # Locates the address element shown at the top of the home page via its resource ID
                                    # Once located, a check is made to ensure the element is visible

                                    paymentNavBtn = driver.find_element_by_id("payments")
                                    paymentNavBtn.click()
                                    # Locates the payments Navigation button element via its resource ID
                                    # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                                    # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                                    homeNavBtn = driver.find_element_by_id("home")
                                    homeNavBtn.click()
                                    # Locates the home Navigation button element via its resource ID
                                    # Once located, the element is clicked

                                    retryBtn = driver.find_element_by_id("retry")
                                    retryBtn.click()
                                    # Locates Retry button element via the elements resource ID.
                                    # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.visibility_of_element_located((By.ID, "layout_data")))
                                    # Explicitly waits for the supply card element to be located via it's resource ID

                                except:
                                    try:

                                        for u in range(10):
                                            WebDriverWait(driver, 5).until(
                                                EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                            # The driver will be explicitly instructed to wait until the progress bar is hidden

                                            retryBtn = driver.find_element_by_id("retry")
                                            retryBtn.click()
                                            # Locates Retry button element via the elements resource ID.
                                            # Once located, the element is clicked

                                        WebDriverWait(driver, 10).until(
                                            EC.visibility_of_element_located((By.ID, "layout_data")))
                                        # Explicitly waits for the supply card element to be located via it's resource ID

                                    except:
                                        try:

                                            WebDriverWait(driver, 10).until(
                                                EC.visibility_of_element_located((By.ID, "layout_data")))
                                            # Explicitly waits for the supply card element to be located via it's resource ID

                                        except(TimeoutError, Exception):
                                            raise Exception(
                                                "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                            # If the home page does not load an except clause attempts to look for the connection error and retry button.

            except:
                raise Exception("Sign in attempt failed. One retry attempt also failed")
                # An Exception error is raised if the sign in attempts fail. As the scenario can not be completed

    except:
        if email.is_displayed():
            # Locates the email field element via the elements resource ID

            WebDriverWait(driver, 5).until_not(EC.staleness_of(email))
            emailNotStale = driver.find_element_by_id("fragment_login__edittext_email")
            # A stale element error was occurring when the send keys command was used.
            # To handle this. The driver is explicitly instructed to wait for the element to no longer be stale

            emailNotStale.send_keys(EmailAddress)
            # Implicitly waits for the field to be ready for interaction.
            # The email address from the dataset example will be inserted into this element via the send keys command.
            # Mobile Keyboard is then hidden

            passwordField = driver.find_element_by_id("fragment_login__edittext_password")
            # Locates the password field element via the elements resource ID.

            passwordField.send_keys(Password)
            driver.hide_keyboard()
            # The password from the dataset example will be inserted into this element via the send keys command.
            # Mobile Keyboard is then hidden

            signInBtn.click()
            # Keyboard may not be closed. Driver is told to implicitly wait for this element to be intractable
            # Finds and Clicks sign in button.

            try:

                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
                # Tries to explicitly wait for the home navigation button to become visible
                try:

                    WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
                    # Explicitly waits for the Retry button element to be located via it's resource ID

                    retryBtn = driver.find_element_by_id("retry")
                    retryBtn.click()
                    # Locates Retry button element via the elements resource ID.
                    # Once located, the element is clicked

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                    # Explicitly waits for the supply card element to be located via it's resource ID

                except:
                    try:

                        for i in range(10):
                            WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                            # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Locates Retry button element via the elements resource ID.
                            # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                        # Explicitly waits for the supply card element to be located via it's resource ID

                    except:
                        try:

                            retryBtn = driver.find_element_by_id("retry")
                            retryBtn.click()
                            # Locates Retry button element via the elements resource ID.
                            # Once located, the element is clicked

                            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                            # Explicitly waits for the supply card element to be located via it's resource ID

                        except:
                            try:

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                # Explicitly waits for the supply card element to be located via it's resource ID

                            except:
                                try:

                                    addressBar = driver.find_element_by_id("text_address")
                                    addressBar.is_displayed()
                                    # Tries to determine if the home before check has completed.
                                    # Locates the address element shown at the top of the home page via its resource ID
                                    # Once located, a check is made to ensure the element is visible

                                    paymentNavBtn = driver.find_element_by_id("payments")
                                    paymentNavBtn.click()
                                    # Locates the payments Navigation button element via its resource ID
                                    # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                                    # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                                    homeNavBtn = driver.find_element_by_id("home")
                                    homeNavBtn.click()
                                    # Locates the home Navigation button element via its resource ID
                                    # Once located, the element is clicked

                                    retryBtn = driver.find_element_by_id("retry")
                                    retryBtn.click()
                                    # Locates Retry button element via the elements resource ID.
                                    # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.visibility_of_element_located((By.ID, "layout_data")))
                                    # Explicitly waits for the supply card element to be located via it's resource ID

                                except:
                                    try:

                                        for u in range(10):
                                            WebDriverWait(driver, 5).until(
                                                EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                            # The driver will be explicitly instructed to wait until the progress bar is hidden

                                            retryBtn = driver.find_element_by_id("retry")
                                            retryBtn.click()
                                            # Locates Retry button element via the elements resource ID.
                                            # Once located, the element is clicked

                                        WebDriverWait(driver, 10).until(
                                            EC.visibility_of_element_located((By.ID, "layout_data")))
                                        # Explicitly waits for the supply card element to be located via it's resource ID

                                    except:
                                        try:

                                            WebDriverWait(driver, 10).until(
                                                EC.visibility_of_element_located((By.ID, "layout_data")))
                                            # Explicitly waits for the supply card element to be located via it's resource ID

                                        except(TimeoutError, Exception):
                                            raise Exception(
                                                "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                            # If the home page does not load an except clause attempts to look for the connection error and retry button.

            except:
                try:

                    signInError = driver.find_element_by_id("errorText")
                    signInError.is_displayed()
                    # Tries to locate the error message that appears when the sign in attempt fails.
                    # Once located, aa check is made to ensure the element is currently visible

                    signInBtn.click()
                    # Locates the sign in button element and clicks this to retry the sign in attempt

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
                    # Explicitly waits for the home navigation button element to become visible via the elements resource ID

                    try:

                        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
                        # Explicitly waits for the Retry button element to be located via it's resource ID

                        retryBtn = driver.find_element_by_id("retry")
                        retryBtn.click()
                        # Locates Retry button element via the elements resource ID.
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                        # Explicitly waits for the supply card element to be located via it's resource ID

                    except:
                        try:

                            for i in range(10):
                                WebDriverWait(driver, 5).until(
                                    EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                            # Explicitly waits for the supply card element to be located via it's resource ID

                        except:
                            try:

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                # Explicitly waits for the supply card element to be located via it's resource ID

                            except:
                                try:
                                    retryBtn = driver.find_element_by_id("retry")
                                    retryBtn.click()
                                    # Locates Retry button element via the elements resource ID.
                                    # Once located, the element is clicked

                                    WebDriverWait(driver, 10).until(
                                        EC.visibility_of_element_located((By.ID, "layout_data")))
                                    # Explicitly waits for the supply card element to be located via it's resource ID

                                except:
                                    try:

                                        addressBar = driver.find_element_by_id("text_address")
                                        addressBar.is_displayed()
                                        # Tries to determine if the home before check has completed.
                                        # Locates the address element shown at the top of the home page via its resource ID
                                        # Once located, a check is made to ensure the element is visible

                                        paymentNavBtn = driver.find_element_by_id("payments")
                                        paymentNavBtn.click()
                                        # Locates the payments Navigation button element via its resource ID
                                        # Once located, the element is clicked

                                        WebDriverWait(driver, 10).until(
                                            EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                                        # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                                        homeNavBtn = driver.find_element_by_id("home")
                                        homeNavBtn.click()
                                        # Locates the home Navigation button element via its resource ID
                                        # Once located, the element is clicked

                                        retryBtn = driver.find_element_by_id("retry")
                                        retryBtn.click()
                                        # Locates Retry button element via the elements resource ID.
                                        # Once located, the element is clicked

                                        WebDriverWait(driver, 10).until(
                                            EC.visibility_of_element_located((By.ID, "layout_data")))
                                        # Explicitly waits for the supply card element to be located via it's resource ID

                                    except:
                                        try:

                                            for u in range(10):
                                                WebDriverWait(driver, 5).until(
                                                    EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                                # The driver will be explicitly instructed to wait until the progress bar is hidden

                                                retryBtn = driver.find_element_by_id("retry")
                                                retryBtn.click()
                                                # Locates Retry button element via the elements resource ID.
                                                # Once located, the element is clicked

                                            WebDriverWait(driver, 10).until(
                                                EC.visibility_of_element_located((By.ID, "layout_data")))
                                            # Explicitly waits for the supply card element to be located via it's resource ID

                                        except:
                                            try:

                                                WebDriverWait(driver, 10).until(
                                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                                # Explicitly waits for the supply card element to be located via it's resource ID

                                            except(TimeoutError, Exception):
                                                raise Exception(
                                                    "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                                # If the home page does not load an except clause attempts to look for the connection error and retry button.

                except:
                    raise Exception("Sign in attempt failed. One retry attempt also failed")
                    # An Exception error is raised if the sign in attempts fail. As the scenario can not be completed
                    # If the email field is displayed then the rewards pop up did not show

        else:
            raise Exception("Rewards Pop could not be located and closed. Email address field could also not be located")
        # Raises an exception if the email field and pop up message can not be located


def scroll_down_to_element(FieldID: str):
    CorrectField = False
    # False boolean value used for loop that locates the fields location

    while not CorrectField:
        Field = driver.find_elements_by_id(FieldID)
        # Determines whether the postcode field is currently visible on the screen via the elements resource ID

        if Field.__len__() == 0:
            driver.swipe(start_x=20, start_y=900, end_x=20, end_y=600)
            # If the field can not be found on the screen.
            # A swipe action is used to scroll down until the element is located

        else:
            CorrectField = True
            # Field has been located and the loop ends

            driver.swipe(start_x=20, start_y=800, end_x=20, end_y=600)
            # Swipe action scrolls down the page to ensure the element is visible


def scroll_up_to_element(FieldID: str):
    CorrectField = False
    # False boolean value used for loop that locates the fields location

    while not CorrectField:
        Field = driver.find_elements_by_id(FieldID)
        # Determines whether the postcode field is currently visible on the screen via the elements resource ID

        if Field.__len__() == 0:
            driver.swipe(start_x=20, start_y=600, end_x=20, end_y=900)
            # If the field can not be found on the screen.
            # A swipe action is used to scroll down until the element is located

        else:
            CorrectField = True
            # Field has been located and the loop ends

            driver.swipe(start_x=20, start_y=600, end_x=20, end_y=800)
            # Swipe action scrolls up the page to ensure the element is visible


def waiting_for_home_screen():
    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "home")))
    # Tries to explicitly wait for the home navigation button to become visible

    try:

        WebDriverWait(driver, 10).until(EC.presence_of_element_located((By.ID, "retry")))
        # Explicitly waits for the Retry button element to be located via it's resource ID

        retryBtn = driver.find_element_by_id("retry")
        retryBtn.click()
        # Locates Retry button element via the elements resource ID. Once located, the element is clicked

        WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
        # Explicitly waits for the supply card element to be located via it's resource ID

    except:
        try:

            for i in range(10):
                WebDriverWait(driver, 5).until(EC.invisibility_of_element_located((By.ID, "progress_bar")))
                # While in range of ten. The driver will be explicitly instructed to wait until the progress bar is hidden

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Locates Retry button element via the elements resource ID. Once located, the element is clicked

            WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
            # Explicitly waits for the supply card element to be located via it's resource ID

        except:
            try:

                retryBtn = driver.find_element_by_id("retry")
                retryBtn.click()
                # Locates Retry button element via the elements resource ID. Once located, the element is clicked

                WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                # Explicitly waits for the supply card element to be located via it's resource ID

            except:
                try:

                    retryBtn = driver.find_element_by_id("retry")
                    retryBtn.click()
                    # Locates Retry button element via the elements resource ID.
                    # Once located, the element is clicked

                    WebDriverWait(driver, 10).until(EC.visibility_of_element_located((By.ID, "layout_data")))
                    # Explicitly waits for the supply card element to be located via it's resource ID

                except:
                    try:

                        addressBar = driver.find_element_by_id("text_address")
                        addressBar.is_displayed()
                        # Tries to determine if the home before check has completed.
                        # Locates the address element shown at the top of the home page via its resource ID
                        # Once located, a check is made to ensure the element is visible

                        paymentNavBtn = driver.find_element_by_id("payments")
                        paymentNavBtn.click()
                        # Locates the payments Navigation button element via its resource ID
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(
                            EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                        # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                        homeNavBtn = driver.find_element_by_id("home")
                        homeNavBtn.click()
                        # Locates the home Navigation button element via its resource ID
                        # Once located, the element is clicked

                        retryBtn = driver.find_element_by_id("retry")
                        retryBtn.click()
                        # Locates Retry button element via the elements resource ID.
                        # Once located, the element is clicked

                        WebDriverWait(driver, 10).until(
                            EC.visibility_of_element_located((By.ID, "layout_data")))
                        # Explicitly waits for the supply card element to be located via it's resource ID

                    except:
                        try:

                            for u in range(10):
                                WebDriverWait(driver, 5).until(
                                    EC.invisibility_of_element_located((By.ID, "progress_bar")))
                                # The driver will be explicitly instructed to wait until the progress bar is hidden

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                            WebDriverWait(driver, 10).until(
                                EC.visibility_of_element_located((By.ID, "layout_data")))
                            # Explicitly waits for the supply card element to be located via it's resource ID

                        except:
                            try:
                                addressBar = driver.find_element_by_id("text_address")
                                addressBar.is_displayed()
                                # Tries to determine if the home before check has completed.
                                # Locates the address element shown at the top of the home page via its resource ID
                                # Once located, a check is made to ensure the element is visible

                                paymentNavBtn = driver.find_element_by_id("payments")
                                paymentNavBtn.click()
                                # Locates the payments Navigation button element via its resource ID
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.presence_of_element_located((By.ID, "component_menu_button_arrow_icon")))
                                # Explicitly waits for the payment option arrow icon element to be located via it's resource ID

                                homeNavBtn = driver.find_element_by_id("home")
                                homeNavBtn.click()
                                # Locates the home Navigation button element via its resource ID
                                # Once located, the element is clicked

                                retryBtn = driver.find_element_by_id("retry")
                                retryBtn.click()
                                # Locates Retry button element via the elements resource ID.
                                # Once located, the element is clicked

                                WebDriverWait(driver, 10).until(
                                    EC.visibility_of_element_located((By.ID, "layout_data")))
                                # Explicitly waits for the supply card element to be located via it's resource ID

                            except:
                                try:

                                    WebDriverWait(driver, 10).until(
                                        EC.visibility_of_element_located((By.ID, "layout_data")))
                                    # Explicitly waits for the supply card element to be located via it's resource ID

                                except(TimeoutError, Exception):
                                    raise Exception(
                                        "Home page did not load. Known issue. Please refer to tickets MUA-653 and MUA-656")
                                    # If the home page does not load an except clause attempts to look for the connection error and retry button.
