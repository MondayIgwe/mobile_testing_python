from behave import *
from Features.Env import driver


@fixture
def before_scenario(context, scenario):
    driver.launch_app()


@fixture
def after_scenario(context, scenario):
    driver.close_app()


@fixture
def after_feature(context, feature):
    driver.quit()
