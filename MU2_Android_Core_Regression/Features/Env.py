from appium import webdriver

desired_caps = {
    'platformName': 'Android',
    'deviceName': 'anyname',
    'app': r'C:/Automation_Apps/Mobile_app/Android/app-dev-debug.apk',
    'automationName': 'Appium'}

driver = webdriver.Remote('http://localhost:4723/wd/hub', desired_caps)
