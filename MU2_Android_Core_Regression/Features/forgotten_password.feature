@JIRA-TTA-204
Feature: 1.2 - Forgotten Password
    As a non sign in user

    In order to recovery my password

    I want to start recovery password procedure

  @done @AwaitingDatabaseAccess
  Scenario Outline: Password recovery - successful
    Given password recovery screen is showing
    And recovery email field is filled with "<Email>"
    And user presses 'Reset Password' button
    Then the message: 'If the email you provided is valid, an email will be sent to "<Email>" with a link to reset your password' is shown
    And link back to Sign in screen shows
#    And user receives an email with a reset link
#    When user clicks reset link
#    Then Reset password screen shows
#    When user inputs new password and clicks OK
#    Then password is reset
#    And link back to Sign in screen shows
#    Then user clicks Sign in screen link
#    And user inputs "<Email>" into email field
#    And user inputs new password
#    And clicks sign in button
#    Then Home screen shows

    Examples:
      | Email                     |
      | testaddress@utilita.co.uk |

  @done
  Scenario: Navigation to Create Account
    Given password recovery screen is showing
    And user clicks create account link
    Then register user screen shows

  @done
  Scenario: Navigation to Sign in page
    Given My Utilita is loaded
    When User selects Forgotten Password link
    Then Forgotten Password page becomes visible
    When User remembers their password and Selects Sign in page link
    Then Sign page becomes visible
